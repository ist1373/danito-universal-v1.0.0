import { Component, OnInit } from '@angular/core';
import {isPlatformBrowser} from "@angular/common";
import {CacheService} from "ng2-cache-service";

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.scss'],
  providers:[CacheService]
})
export class AboutusComponent implements OnInit {

  constructor(private cacheService:CacheService) { }

  ngOnInit() {
    if (isPlatformBrowser(this.cacheService.get('platform'))) {
      window.scrollTo(0, 0);
    }
  }

}
