import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Content, ProductState} from "../../entity/Content";
import {AppContextUrl, FileContextUrl} from "../../globals";
import {BookletService} from "../../service/booklet.service";


@Component({
  selector: 'app-admin-booklet',
  templateUrl: './admin-booklet.component.html',
  styleUrls: ['./admin-booklet.component.scss'],
  providers:[BookletService]
})
export class AdminBookletComponent implements OnInit {

  @Input() booklet:Content;
  base = AppContextUrl;
  fileBase = FileContextUrl;

  @Output() onChangeState: EventEmitter<Content> = new EventEmitter();

  JozveStates: Array<any>;
  ProductState: typeof ProductState = ProductState;
  newState:ProductState ;

  constructor(private bookletService:BookletService){

  }

  ngOnInit() {
    this.JozveStates = [
      {value: '0', label: 'درحال بررسی'},
      {value: '1', label: 'منتشر شده'},
      {value: '2', label: 'لغو شده'},
      {value: '3', label: 'نیاز به تغییر'},
      {value: '4', label: 'آرشیو شده'}
    ];
    this.newState= this.booklet.productState;
  }

  changeJozveState(event:any,booklet:Content)
  {

    this.newState = event.value;
  }
  editJozveState(booklet:Content){

    this.bookletService.editJozveState(booklet.id,this.newState).subscribe((res)=>{
      console.log(res);
      this.onChangeState.emit(booklet);
    });
  }



}
