import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {UniversityService} from "../../service/university.service";
import {University} from "../../entity/University";
import {CacheService} from "ng2-cache-service";
import {number} from "ng2-validation/dist/number";
import {strictEqual} from "assert";
import {AppContextUrl, FileContextUrl} from "../../globals";
import {ProgressHttp} from "angular-progress-http";
import {RequestOptions,Headers,Response} from "@angular/http";
import {AccessType, UploadedFile} from "../../entity/UploadedFile";
import {Content, ContentType} from "../../entity/Content";
import {CategoryService} from "../../category-dialog/category-service";
import {MdDialog, MdSnackBar, MdSnackBarConfig} from "@angular/material";
import {CategoryDialogComponent} from "../../category-dialog/category-dialog.component";
import {plainToClass} from "class-transformer";
import {Category} from "../../entity/Category";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {User} from "../../entity/User";
import {TagService} from "../../service/tag.service";
import {Tag} from "../../entity/Tag";
import {TagContentType} from "@angular/compiler";
import {BookletService} from "../../service/booklet.service";
import {ActivatedRoute} from "@angular/router";
import {GalleryService} from "ng-gallery";
import {isPlatformBrowser} from "@angular/common";





@Component({
  selector: 'app-new-booklet',
  templateUrl: './modify-booklet.component.html',
  styleUrls: ['./modify-booklet.component.scss'],
  providers:[UniversityService,CacheService,CategoryService,BookletService,TagService]
})
export class ModifyBookletComponent implements OnInit {
  modificationType:ModificationType;
  category:Category;
  content:Content;
  universities : Array<any>=[];
  files:File[] = [];//new files have to be uploaded
  existedFiles:UploadedFile[]=[]
  sampleImages:File[]=[];
  fileProgresses:ProgressOfUpload[]=[];
  imagesProgresses:ProgressOfUpload[]=[];
  fileAccessType : Array<any>;
  private value:any = {};


  FileType: typeof FileType = FileType;
  ModificationType: typeof ModificationType = ModificationType;
  AccessType: typeof AccessType = AccessType;


  jozveForm:FormGroup;
  title = new FormControl('', Validators.required);
  price = new FormControl('', Validators.required);
  // writer = new FormControl('');
  writeYear = new FormControl('');
  pages = new FormControl('');
  prof = new FormControl('');
  desc = new FormControl('');
  base = AppContextUrl;
  fileBase = FileContextUrl;
  token = this.cacheService.get("token");
  sampleGalleryImages=[];
  productState = [];


  public ngModelTags = [];//tags to save by tag-input
  public autocompleteBookletTags = [];//tags to offer in html


  // @ViewChild('uniSelect') uniSelect:ElementRef;

  constructor(private universityService:UniversityService, private cacheService:CacheService,
              private http: ProgressHttp, private categoryService:CategoryService, public dialog: MdDialog,
              private bookletService:BookletService, public snackBar: MdSnackBar, private tagService:TagService,
              private activatedRoute:ActivatedRoute,public gallery:GalleryService) {

    this.jozveForm=new FormGroup({
      title:this.title,
      price: this.price,
      // writer: this.writer,
      writeYear:this.writeYear,
      pages:this.pages,
      prof:this.prof,
      desc:this.desc});
  }




  ngOnInit() {
    if (isPlatformBrowser(this.cacheService.get('platform'))) {
      window.scrollTo(0, 0);
    }
    this.initUniversirsities();
    this.content = new Content();
    this.activatedRoute.url.subscribe((url)=>{
      if(url[2].toString()=="new")
      {

        this.modificationType = ModificationType.New;
      }
      else
      {


        this.modificationType = ModificationType.Edit;
        this.bookletService.getContentByID(Number(url[3])).subscribe((booklet)=>{
          this.content = booklet;
          console.log(booklet);
          this.initSampleImages();
          this.title.setValue(booklet.title) ;
          this.price.setValue(booklet.price);
          this.writeYear.setValue(booklet.writeYear);
          // this.writer.setValue(booklet.writer);
          this.pages.setValue(booklet.pagesNum);
          this.prof.setValue(booklet.professor);
          this.desc.setValue(booklet.description);
          this.category = booklet.category;
          for(let tag of booklet.tags)
            this.ngModelTags.push({display:tag.tagString,value:tag.id});
          for(let file of booklet.files)
            this.existedFiles.push(file);
        });
      }
    });
    this.fileAccessType = [
      {value: '0', label: 'عمومی'},
      {value: '1', label: 'خصوصی'},
      {value: '2', label: 'دسترسی با پرداخت'},
      {value: '3', label: 'لغو شده'}
    ];

    // Evaluating, Published, NeedChange, Canceled, Archived
    this.productState = [
      {value: '0', label: 'در حال بررسی'},
      {value: '1', label: 'منتشر شده'},
      {value: '2', label: 'نیاز به تغییر'},
      {value: '3', label: 'لغو شده'},
      {value: '3', label: 'آرشیو شده'}
    ];
  }
  selectProductState(item)
  {
    this.content.productState = item.value;
  }

  public selectUniversity(item:any):void {
    console.log('Selected value is: ', item.value);
    let uni = new University();
    uni.id = item.value;
    this.content.university = uni;
  }

  initSampleImages()
  {
    for(let image of this.content.images)
    {
      let i={
        src:FileContextUrl+'fid='+image.id,
        thumbnail: FileContextUrl+'fid='+image.id+ '&isThumb=true',
        text: image.description
      };
      this.sampleGalleryImages.push(i);
    }
    console.log(this.sampleGalleryImages);
    this.gallery.load(this.sampleGalleryImages);
  }

  openFileBrowser(event,fileType:FileType)
  {
    if(fileType == FileType.Jozve)
    {
      for(let file of event.target.files)
      {
        this.files.push(file);
        this.fileProgresses.push(new ProgressOfUpload());
      }
    }
    else
    {
      for(let file of event.target.files)
      {
        this.sampleImages.push(file);
        this.imagesProgresses.push(new ProgressOfUpload());
      }
    }


  }




  uploadFile(file:File,fileType:FileType,index:number)
  {

    var uploadUrl= AppContextUrl+'file/upload';

    let formData:FormData = new FormData();
    formData.append('file', file, file.name);
    if(this.content.price==0||fileType==FileType.Image)
    {
      formData.append('accessType', 'Public' );
    }
    else
    {
      formData.append('accessType', 'AccessOnPay' );
    }
    let headers = new Headers({});
    headers.set("Authorization",this.cacheService.get("token"));
    let options = new RequestOptions({ headers: headers});

    this.http
      .withUploadProgressListener(progress => {
        console.log(progress);
        if(fileType==FileType.Jozve)
          this.fileProgresses[index].progress=progress.percentage;
        else
          this.imagesProgresses[index].progress=progress.percentage;
      })
      .post(uploadUrl,formData,options)
      .subscribe((response) => {
        // console.log(response);

        var uploadedFile = new UploadedFile();
        uploadedFile.id = response.json().code;
        if(fileType==FileType.Jozve)
        {
          this.content.files.push(uploadedFile);
          this.fileProgresses[index].complete = true;
        }
        else
        {
          this.content.images.push(uploadedFile);
          this.imagesProgresses[index].complete=true;
        }

      })
  }


  openCategoryModal(){
    this.categoryService.getParentsCategory().subscribe((data:Response)=> {
      let dialogRef = this.dialog.open(CategoryDialogComponent);
      dialogRef.componentInstance.returnCatId = true;
      dialogRef.componentInstance.categories = plainToClass(Category, data.json());
      dialogRef.componentInstance.categoryHeader ="دسته بندی";
    });
  }

  ngAfterViewInit(){
    CategoryService.categoryObserver.subscribe((cat:Category)=>{
      this.category = cat;
      this.content.category = cat
    });
  }

  saveJozve()
  {

     this.content.contentType = ContentType.Booklet;
     this.content.title = this.title.value;
     this.content.price = this.price.value;
     this.content.pagesNum = this.pages.value as number;
     this.content.writeYear = this.writeYear.value;
     this.content.professor = this.prof.value;
     let user:User = this.cacheService.get("user");
     this.content.description = this.desc.value;
     console.log(this.content);
     this.bookletService.saveJozve(this.content).subscribe((res)=>{
        console.log(res);
        if(res.json()['success']==true)
        {
          this.openSnackBar("ذخیره جزوه با موفقیت انجام شد","success");
          this.clearForms();
        }

        else
          this.openSnackBar(res.json()['message'],"error");
     });
  }

  editJozve(){
    console.log(this.content);
    this.content.title = this.title.value;
    this.content.price = this.price.value;
    // this.book.writer = this.writer.value;
    this.content.pagesNum = this.pages.value as number;
    this.content.writeYear = this.writeYear.value;
    let user:User = this.cacheService.get("user");
    this.content.description = this.desc.value;
    this.bookletService.editJozve(this.content).subscribe((res)=>{
      console.log(res);
      if(res.json()['success']==true)
        this.openSnackBar("ذخیره جزوه با موفقیت انجام شد","success");
      else
        this.openSnackBar(res.json()['message'],"error");
    });
  }
  // this.openSnackBar(res.json()['message'],"error");
  openSnackBar(message:string,cssClass:string) {
    var conf = new MdSnackBarConfig();
    conf.duration = 5000;
    conf.extraClasses = [cssClass];
    this.snackBar.open(message,"",conf);
  }

  initUniversirsities()
  {
        this.universityService.getUniversities().subscribe((unis:University[])=>{
          for(let uni of unis)
          {
            this.universities.push({value:uni.id,label:uni.title.toString()});
          }
        });
        this.cacheService.set("unis",this.universities);
  }

  getAutoCompleteTags(tagStr)
  {
    if(tagStr.length>0)
    {
      this.tagService.getSimilarTages(tagStr).subscribe((tags:Tag[])=>{
        this.autocompleteBookletTags = [];
        for (let tag of tags)
        {
          // this.tags.set(tag.tagString,tag.id);
          let atag = {display:tag.tagString,value:tag.id}

          this.autocompleteBookletTags.push(atag);
        }
      })
    }
  }

  onRemoveTag(removeTag){
    console.log(removeTag);
    for(let i =0;i<this.content.tags.length;i++ )
    {
      let tag = this.content.tags[i];
      if(tag.tagString == removeTag.display)
      {
        this.content.tags.splice(i,1);

      }
    }
  }

  onAddTag(addedTag)
  {
        console.log(addedTag);
        let tag = new Tag();
        // tag.id = this.tags.get(addedTag.display);
        tag.tagString = addedTag.display;
        tag.id = addedTag.value;
        this.content.tags.push(tag);
  }



  changeFileAcessType(event,i:number)
  {

     this.content.files[i].accessType = this.fileAccessType[event.value].label;
  }
  changeImageAcessType(event,i:number)
  {
    this.content.images[i].accessType = this.fileAccessType[event.value].label;
  }

  clearForms(){
    this.title.setValue('');
    this.pages.setValue('');
    this.prof.setValue('');
    this.desc.setValue('');
    this.ngModelTags = [];
    this.category = null;
    this.files = [];
    this.price.setValue('');
    this.writeYear.setValue('');
    this.sampleImages = [];
    this.imagesProgresses = [];
    this.content = new Content();
  }

}






export enum FileType{Jozve,Image};
export enum ModificationType{Edit,New};

export class ProgressOfUpload{
   progress:number=0;
   complete:boolean=false;
   constructor(){
     this.progress = 0;
     this.complete = false;
   }

}
