import { Component, OnInit } from '@angular/core';
import {Comment, CommentState} from "../../entity/Comment";
import {CommentsService} from "../../service/comments.service";
import {SelectComponent} from "ng-select";
import {isPlatformBrowser} from "@angular/common";
import {CacheService} from "ng2-cache-service";


@Component({
  selector: 'app-comments-list',
  templateUrl: './comments-list.component.html',
  styleUrls: ['./comments-list.component.scss'],
  providers:[CommentsService,CacheService]
})
export class CommentsListComponent implements OnInit {

  comments:Comment[];
  CommentStates:Array<any>;
  showCommentState:CommentState;
  page = 0;
  showMore = true;
  CommentState:typeof CommentState=CommentState;


  constructor(private commentsService:CommentsService,private cacheService:CacheService) { }

  ngOnInit() {
    if (isPlatformBrowser(this.cacheService.get('platform'))) {
      window.scrollTo(0, 0);
    }
    this.showCommentState = CommentState.Pending;
    this.CommentStates = [
      {value: '0', label: 'معوق'},
      {value: '1', label: 'منتشر شده'},
      {value: '2', label: 'لغو شده'}
    ];

    this.getComments(this.showCommentState);

  }


  getComments(state:CommentState)
  {
    this.commentsService.getByState(state).subscribe((comments)=>{
        this.comments = comments;
        console.log(this.comments);
    });
  }

  changeCommentsState(event)
  {
    this.page = 0;
    this.showMore = true
    this.showCommentState = event.value;
    this.getComments(this.showCommentState);
  }


  changeCommentState(event,c:Comment)
  {

      c.commentState = event.value;


  }

  editComment(comment:Comment)
  {

     this.commentsService.editComment(comment).subscribe((res)=>{
       console.log(res);
       this.comments = this.comments.filter(item=>item!=comment);
     });
  }

}
