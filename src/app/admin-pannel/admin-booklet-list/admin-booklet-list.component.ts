import { Component, OnInit } from '@angular/core';
import {BookletService} from "../../service/booklet.service";
import {Content, ProductState} from "../../entity/Content";
import {CategoryService} from "../../category-dialog/category-service";
import {MdDialog} from "@angular/material";
import {CategoryDialogComponent} from "../../category-dialog/category-dialog.component";
import {plainToClass} from "class-transformer";
import {Category} from "../../entity/Category";
import {Response} from "@angular/http";

@Component({
  selector: 'app-admin-booklet-list',
  templateUrl: './admin-booklet-list.component.html',
  styleUrls: ['./admin-booklet-list.component.scss'],
  providers:[BookletService,CategoryService]
})
export class AdminBookletListComponent implements OnInit {
  JozveStates: Array<any>;
  booklets:Array<Content>=[];
  category:Category;
  showProductState:ProductState = ProductState.Evaluating;
  categoryFilter=false;
  page = 0 ;
  showMore = true;

  constructor(private bookletsService:BookletService,private categoryService:CategoryService,public dialog: MdDialog) {
  }

  ngOnInit() {

    this.JozveStates = [
      {value: '0', label: 'درحال بررسی'},
      {value: '1', label: 'منتشر شده'},
      {value: '2', label: 'لغو شده'},
      {value: '3', label: 'نیاز به تغییر'},
      {value: '4', label: 'آرشیو شده'}
    ];
    this.getBookletsByState(this.showProductState,this.page,false);
  }


  getBookletsByState(state:ProductState,page:number,more:boolean){
     this.bookletsService.getContentsByState(state,page).subscribe((booklets)=>{
       if(more)
       {
         if(booklets.length == 0)
         {
           this.showMore = false;
         }
         else
         {
           for(let booklet of booklets)
             this.booklets.push( booklet);
           console.log(booklets);
         }
       }
       else
       {
         this.booklets = booklets;
       }

     });
  }

  getBookletsByStateAndCategory(state:ProductState,catid:number,page:number,more:boolean){
    this.bookletsService.getContentsByStateAndCategory(state,catid,page).subscribe((booklets)=>{
      if(more) {
        if (booklets.length == 0) {
          this.showMore = false;
        }
        else {
          for (let booklet of booklets)
            this.booklets.push(booklet);
          console.log(booklets);
        }
      }
      else
      {
        this.booklets = booklets;
      }
    });
  }



  changeBookletsState(event)
  {
    this.page = 0;
    this.showMore = true
    this.showProductState = event.value;
    if(this.categoryFilter)
      this.getBookletsByStateAndCategory(this.showProductState,this.category.id,this.page,false);
    else
      this.getBookletsByState(event.value,this.page,false);
  }
  changeContentState(event){
    console.log(event);
    this.booklets = this.booklets.filter(item=>item!=event)
  }


  openCategoryModal(){
    this.categoryService.getParentsCategory().subscribe((data:Response)=> {
      let dialogRef = this.dialog.open(CategoryDialogComponent);
      dialogRef.componentInstance.returnCatId = true;
      dialogRef.componentInstance.categories = plainToClass(Category, data.json());
      dialogRef.componentInstance.categoryHeader ="دسته بندی";
    });
  }
  ngAfterViewInit(){
    CategoryService.categoryObserver.subscribe((cat:Category)=>{
      this.page = 0;
      this.showMore = true;
      this.category = cat;
      this.getBookletsByStateAndCategory(this.showProductState,this.category.id,this.page,false);
    });
  }


  categorySelection(event)
  {
    this.categoryFilter = event.checked;
    if(this.category!=null&&this.categoryFilter)
    {
      this.getBookletsByStateAndCategory(this.showProductState,this.category.id,this.page,false);
    }
    else
    {
      this.getBookletsByState(this.showProductState,this.page,false);
    }
  }

  loadMore()
  {
    this.page++;
    if(this.category!=null&&this.categoryFilter)
    {
      this.getBookletsByStateAndCategory(this.showProductState,this.category.id,this.page,true);
    }
    else
    {
      this.getBookletsByState(this.showProductState,this.page,true);
    }

  }

}
