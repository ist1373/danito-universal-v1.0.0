import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {AppContextUrl} from "../globals";
import {Subject} from "rxjs/Subject";
import {Category} from "../entity/Category";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Injectable()
export class CategoryService {

  constructor(private http:Http) {

  }
  static categoryObserver = new Subject<Category>();

  // getCategoryObserver()
  // {
  //   return this.categoryObserver.asObservable();
  // }

  getParentsCategory()
  {
    return this.http.get(AppContextUrl + "/category")
  }

  getSubCategories(id:number)
  {
    return this.http.get(AppContextUrl + "/category?categoryId="+id);
  }


}
