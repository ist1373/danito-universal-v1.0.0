var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { AppContextUrl } from "../globals";
import { Subject } from "rxjs/Subject";
var CategoryService = (function () {
    function CategoryService(http) {
        this.http = http;
    }
    // getCategoryObserver()
    // {
    //   return this.categoryObserver.asObservable();
    // }
    CategoryService.prototype.getParentsCategory = function () {
        return this.http.get(AppContextUrl + "/category");
    };
    CategoryService.prototype.getSubCategories = function (id) {
        return this.http.get(AppContextUrl + "/category?categoryId=" + id);
    };
    return CategoryService;
}());
CategoryService.categoryObserver = new Subject();
CategoryService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Http])
], CategoryService);
export { CategoryService };
//# sourceMappingURL=category-service.js.map