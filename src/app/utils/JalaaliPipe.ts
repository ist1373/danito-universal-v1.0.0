/**
 * Created by iman on 7/12/2017.
 */
import { Pipe, PipeTransform } from '@angular/core';

/*
 * Convert bytes into largest possible unit.
 * Takes an precision argument that defaults to 2.
 * Usage:
 *   bytes | fileSize:precision
 * Example:
 *   {{ 1024 |  fileSize}}
 *   formats to: 1 KB
 */
var jalaali = require('jalaali-js');

@Pipe({name: 'jalaaliPipe'})
export class JalaaliPipe implements PipeTransform {



  transform(value:Date) : string {
    console.log(value);
    let jalaliDate = jalaali.toJalaali(value.getFullYear(),value.getMonth(),value.getDay());
    return jalaliDate['jy'] + '/' + jalaliDate['jm'] + '/' + jalaliDate['jd']   // { jy: 1395, jm: 1, jd: 23 }
  }
}
