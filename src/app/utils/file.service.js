var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http, ResponseContentType, Headers, RequestOptions } from "@angular/http";
import { AppContextUrl } from "../globals";
import { DomSanitizer } from "@angular/platform-browser";
import 'rxjs/add/operator/map';
var FileService = (function () {
    function FileService(http, sanitizer) {
        this.http = http;
        this.sanitizer = sanitizer;
    }
    FileService.prototype.getImageWithId = function (imageId) {
        var _this = this;
        var headers = new Headers();
        headers.append('Content-Type', 'image/jpg');
        var options = new RequestOptions({
            headers: headers,
            responseType: ResponseContentType.Blob
        });
        return this.http.get(AppContextUrl + 'file/' + imageId, options).map(function (response) {
            // console.log(response);
            var mediaType = response.headers.get("Content-Type");
            // console.log(mediaType);
            var blob = new Blob([response["_body"]], { type: mediaType });
            var urlCreator = window.URL;
            var url = urlCreator.createObjectURL(blob);
            return _this.sanitizer.bypassSecurityTrustUrl(url);
        });
        ;
    };
    return FileService;
}());
FileService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Http, DomSanitizer])
], FileService);
export { FileService };
//# sourceMappingURL=file.service.js.map