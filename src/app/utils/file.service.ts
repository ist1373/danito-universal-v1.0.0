import { Injectable } from '@angular/core';
import {Http, ResponseContentType, Headers, RequestOptions} from "@angular/http";
import {AppContextUrl} from "../globals";
import {SafeUrl, DomSanitizer} from "@angular/platform-browser";
import 'rxjs/add/operator/map'

@Injectable()
export class FileService {

  constructor(private http:Http,private sanitizer:DomSanitizer) { }


  getImageWithId(imageId:number){

    var headers = new Headers();
    headers.append('Content-Type', 'image/jpg');
    let options = new RequestOptions({
      headers: headers,
      responseType: ResponseContentType.Blob
    });
    return this.http.get(AppContextUrl + 'file/'+imageId,options).map(
      (response) => {
        // console.log(response);
        var mediaType = response.headers.get("Content-Type");
        // console.log(mediaType);
        var blob = new Blob([response["_body"]], {type: mediaType});
        var urlCreator = window.URL;
        var url = urlCreator.createObjectURL(blob);
        return this.sanitizer.bypassSecurityTrustUrl( url );
      });
   ;
  }




}
