var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { SliderSection } from "../entity/model/SliderSection";
import { SliderPage } from "./SliderPage";
import { FileService } from "../utils/file.service";
import { Http } from "@angular/http";
import { AppContextUrl } from "../globals";
import { DomSanitizer } from "@angular/platform-browser";
var SliderComponent = (function () {
    function SliderComponent(fileService, http, sanitizer) {
        this.fileService = fileService;
        this.http = http;
        this.sanitizer = sanitizer;
        this.base = AppContextUrl;
        this.imgurl = "";
        this.sliderPages = new Array();
    }
    SliderComponent.prototype.ngOnInit = function () {
        this.initSlider(this.slider);
    };
    SliderComponent.prototype.initSlider = function (slider) {
        for (var i = 0; i < slider.sliderList.length; i += 3) {
            var sliderPage = new SliderPage();
            for (var j = i; j < i + 3; j++) {
                sliderPage.sliders.push(slider.sliderList[j]);
                // this.getImage(slider.sliderList[j].image.id,slider.sliderList[j]);
            }
            this.sliderPages.push(sliderPage);
        }
    };
    SliderComponent.prototype.getImage = function (id, slider) {
        this.fileService.getImageWithId(id).subscribe(function (safeurl) {
            slider.imageUrl = safeurl;
        });
        // var headers = new Headers();
        // headers.append('Content-Type', 'image/jpg');
        // let options = new RequestOptions({
        //   headers: headers,
        //   responseType: ResponseContentType.Blob
        // });
        // this.http.get(AppContextUrl + 'file/'+3,options).subscribe(
        //   (response) => {
        //     console.log(response);
        //     var mediaType = response.headers.get("Content-Type");
        //     // console.log(mediaType);
        //      var blob = new Blob([response["_body"]], {type: mediaType});
        //      var urlCreator = window.URL;
        //      var url = urlCreator.createObjectURL(blob);
        //      this.imgurl = this.sanitizer.bypassSecurityTrustUrl( url );
        //   });
    };
    return SliderComponent;
}());
__decorate([
    Input(),
    __metadata("design:type", SliderSection)
], SliderComponent.prototype, "slider", void 0);
SliderComponent = __decorate([
    Component({
        selector: 'app-slider',
        templateUrl: './slider.component.html',
        styleUrls: ['./slider.component.scss'],
        providers: [SliderSection, FileService]
    }),
    __metadata("design:paramtypes", [FileService, Http, DomSanitizer])
], SliderComponent);
export { SliderComponent };
//# sourceMappingURL=slider.component.js.map
