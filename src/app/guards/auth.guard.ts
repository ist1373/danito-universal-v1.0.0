import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {CacheService} from "ng2-cache-service";
import {User} from "../entity/User";
import {AuthorityName} from "../entity/security/Authority";
/**
 * Created by iman on 6/13/2017.
 */
@Injectable()
export class AuthGuard implements CanActivate
{
  constructor(private cacheServise:CacheService,private router:Router)
  {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      var user: User = this.cacheServise.get("user");
      if (user!=null)
      for (let authority of user.authorities)
      {
        console.log(authority.name);
        if(authority.name == "ROLE_ADMIN")
        {
          return true;
        }
      }
      this.router.navigate(['/AccessDenied']);
      return false;
  }
}
