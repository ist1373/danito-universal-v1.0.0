
import {Data} from "@angular/router";
import {UploadedFile} from "./UploadedFile";
import {Type} from "class-transformer/index";
import {Authority} from "./security/Authority";
export class User  {


  id:number;

  username:string;

  password:string;

  firstName:string;

  lastName:string;

  email:string;

  phoneNumber:string;

  enabled:boolean;

  @Type(() => Authority)
  authorities:Authority[];

  @Type(() => Date)
  lastPasswordResetDate:Date;


  // authorities:Array<Authority>;
  @Type(() => UploadedFile)
  profileImage:UploadedFile;

}/**
 * Created by iman on 10/02/2017.
 */
