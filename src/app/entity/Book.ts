// import {Product} from "./Product";
import {Category} from './Category';
import {UploadedFile} from './UploadedFile';
import {City} from './City';
import {Tag} from './Tag';
import {Injectable} from '@angular/core';
import {Type} from 'class-transformer/index';
import {User} from './User';
import {CallInfo} from './CallInfo';
import {University} from './University';


/**
 * Created by iman on 10/02/2017.
 */



export class Book  {

  id: number;

  title: String;

  @Type(() => UploadedFile)
  coverImage: UploadedFile;


  @Type(() => Date)
  creationDate: Date;

  @Type(() => Date)
  publishedDate: Date;

  @Type(() => Date)
  archivedDate: Date;


  @Type(() => Category)
  category: Category;

  @Type(() => Category)
  optionalCategories: Category[];

  @Type(() => UploadedFile)
  images: UploadedFile[];

  @Type(() => University)
  university: University;


  @Type(() => City)
  city: City;

  @Type(() => Comment)
  comments: Comment[];

  @Type(() => Tag)
  tags: Tag[];

  author: String ;

  translator: String ;

  publisher: String ;


  publishYear: number ;

  bookPrice: number;

  sellPrice: number;

  offPercentage: number;

  avgRate: number;

  description: String;

  bookState: BookState;

  tagState: TagState;

  weight: number;



  @Type(() => CallInfo)
  callInfo: CallInfo;


    constructor() {
      this.images = [];
      this.tags = [];
    }
}


export enum  BookState{GoodUsed, BadUsed, AlmostNew, New};
export enum TagState {
  Immediate, Rare, UnderPrice, Normal
}
