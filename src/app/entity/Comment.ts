
import {User} from "./User";
import {Type} from "class-transformer/index";
import {Content} from "./Content";
import {Product} from "./Product";
export class Comment {

  id:Number;

  rate:number;

  description:String;

  @Type(() => User)
  createdBy:User;

  @Type(() => Product)
  product:Product;

  @Type(() => Date)
  creationDate:Date;

  // commentState:CommentState;
  commentState:CommentState;
}

 export enum CommentState {Pending, Published, Canceled};
