
import {User} from "./User";
import {Product} from "./Product";
import {Type} from "class-transformer/index";
import {Content} from "./Content";
export class DownloadedContent {

  id:Number;

  @Type(() => Content)
  content:Content;

  @Type(() => User)
  downloadedBy:User;

  @Type(() => Date)
  creationDate:Date;

  deviceId:string;

}


