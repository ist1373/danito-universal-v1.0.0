
import {Book} from "./Book";
import {Type} from "class-transformer/index";
import {Store} from "./Store";
import {BookPlace} from "./BookPlace";
export class CallInfo {

  id:number;

  email:String;


  phone:String;


  messengerId:String;

  infoType:InfoType;

  @Type(() => Book)
  book:Book;

  @Type(() => Store)
  store:Store;

  @Type(() => BookPlace)
  bookPlace:BookPlace;

}


export enum InfoType {
  Personal, Store, Place
}
