
import {Book} from './Book';
import {Type} from 'class-transformer/index';
import {UploadedFile} from './UploadedFile';
import {User} from './User';
import {City} from './City';
import {University} from './University';
import {PlaceState} from "./BookPlace";

export class  Store{

  id:number;

  title:string;


  description:string;


  address:string;

  phoneNumber:string;

  weight:number;

  @Type(() => Book)
  books:Book[];


  @Type(() => UploadedFile)
  coverImage:UploadedFile;

  @Type(() => User)
   createdBy:User;



  @Type(() => City)
  cities:City[];

  @Type(() => University)
  university:University;

  storeState:StoreState;

}


export enum StoreState {Evaluating, Activated, Disabled, NeedChange, Rejected}
