import {Type} from "class-transformer/index";
import {Book} from "./Book";
import {Product} from "./Product";
import {Content} from "./Content";

export class Tag {


   id:number;

   tagString:string;

   @Type(() => Book)
   books:Book[] ;

  @Type(() => Content)
  contents:Content[] ;

}
