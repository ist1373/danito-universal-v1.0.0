var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Category } from "./Category";
import { UploadedFile } from "./UploadedFile";
import { Tag } from "./Tag";
import { Type } from "class-transformer/index";
import { University } from "./University";
import { User } from "./User";
var Jozve = (function () {
    function Jozve() {
        this.files = [];
        this.images = [];
        this.tags = [];
    }
    return Jozve;
}());
export { Jozve };
__decorate([
    Type(function () { return UploadedFile; }),
    __metadata("design:type", UploadedFile)
], Jozve.prototype, "coverImage", void 0);
__decorate([
    Type(function () { return Date; }),
    __metadata("design:type", Date)
], Jozve.prototype, "creationDate", void 0);
__decorate([
    Type(function () { return Date; }),
    __metadata("design:type", Date)
], Jozve.prototype, "publishedDate", void 0);
__decorate([
    Type(function () { return Date; }),
    __metadata("design:type", Date)
], Jozve.prototype, "archivedDate", void 0);
__decorate([
    Type(function () { return Category; }),
    __metadata("design:type", Category)
], Jozve.prototype, "category", void 0);
__decorate([
    Type(function () { return University; }),
    __metadata("design:type", University)
], Jozve.prototype, "university", void 0);
__decorate([
    Type(function () { return UploadedFile; }),
    __metadata("design:type", Array)
], Jozve.prototype, "files", void 0);
__decorate([
    Type(function () { return UploadedFile; }),
    __metadata("design:type", Array)
], Jozve.prototype, "images", void 0);
__decorate([
    Type(function () { return Comment; }),
    __metadata("design:type", Array)
], Jozve.prototype, "comments", void 0);
__decorate([
    Type(function () { return User; }),
    __metadata("design:type", User)
], Jozve.prototype, "createdBy", void 0);
__decorate([
    Type(function () { return Tag; }),
    __metadata("design:type", Array)
], Jozve.prototype, "tags", void 0);
var ProductState;
(function (ProductState) {
    ProductState[ProductState["Evaluating"] = 0] = "Evaluating";
    ProductState[ProductState["Published"] = 1] = "Published";
    ProductState[ProductState["Canceled"] = 2] = "Canceled";
    ProductState[ProductState["NeedChange"] = 3] = "NeedChange";
    ProductState[ProductState["Archived"] = 4] = "Archived";
})(ProductState || (ProductState = {}));
//# sourceMappingURL=Jozve.js.map