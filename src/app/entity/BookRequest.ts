

import {Book} from "./Book";
import {User} from "./User";
import {Type} from "class-transformer/index";
import {Store} from "./Store";
import {BookPlace} from "./BookPlace";


export class BookRequest {


  id:number;

  @Type(() => Book)
  book:Book;

  @Type(() => User)
  createdBy:User;

  @Type(() => Store)
  store:Store;

  @Type(() => BookPlace)
   bookPlace:BookPlace;

  @Type(() => Date)
  dateStart:Date;

  @Type(() => Date)
  dateRespond:Date;

  requestText:String;

  responseText:String;


  suggestedPrice:number;


  requestState:RequestState;

   requestLocation:RequestLocation;
}

export enum RequestState {
   Accepted, Rejected, Pending, Canceled
 }

export enum RequestLocation {
  Store, Place
}
