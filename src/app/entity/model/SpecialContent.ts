import {} from "../Content";
import {UploadedFile} from "../UploadedFile";
import {Type} from "class-transformer/index";
import {Content} from "../Content";
/**
 * Created by iman on 20/02/2017.
 */

export class SpecialContent {
   @Type(() => UploadedFile)
   posterImage:UploadedFile;

   @Type(() => UploadedFile)
   posterImageWeb:UploadedFile;

   @Type(() => Content)
   content:Content;

   position:number;

}
