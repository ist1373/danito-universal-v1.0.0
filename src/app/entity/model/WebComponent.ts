import {Slider} from "../Slider";
import {ContentSection} from "./ContentSection";
import {CategorySection} from "./CategorySection";
import {SpecialContent} from "./SpecialContent";
import {PosterSection} from "./PosterSection";
import {SliderSection} from "./SliderSection";
import {BookSection} from "./BookSection";
import {SpecialBook} from "./SpecialBook";
/**
 * Created by iman on 22/02/2017.
 */
export class WebComponent {

  componentType:WebComponentType;
  sliderSection:SliderSection;
  jozzveSection:ContentSection;
  categorySection:CategorySection;
  specialJozzve:SpecialContent;
  posterSection:PosterSection;
  bookSection:BookSection;
  specialBook:SpecialBook;
  constructor() {
  }

}

export enum WebComponentType{JozzveSection,CategorySection,SpecialJozzve,PosterSection,SliderSection,BookSection,SpecialBook}
