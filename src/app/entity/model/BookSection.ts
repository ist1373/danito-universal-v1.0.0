import {Book} from "../Book";
import {Type} from "class-transformer/index";
/**
 * Created by iman on 16/02/2017.
 */


export class BookSection {

   title:String;

   loadMoreLink:String;

   @Type(() => Book)
   bookList:Book[];

   position:number;


}
