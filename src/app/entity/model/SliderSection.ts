import {Slider} from "../Slider";
import {Type} from "class-transformer/index";
/**
 * Created by iman on 20/02/2017.
 */
export class SliderSection {

  @Type(() => Slider)
  sliderList:Slider[];
  position:number;
}

