import {User} from "./User";
import {Product} from "./Product";
/**
 * Created by iman on 16/02/2017.
 */
export class ProductVisit {


  id:number;


  visitor:User;


  product:Product;


  creationDate:Date;

}
