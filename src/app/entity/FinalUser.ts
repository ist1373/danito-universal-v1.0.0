

import {User} from "./User";
import {UploadedFile} from "./UploadedFile";
import {Type} from "class-transformer";

export class FinalUser extends User {


  deviceId:String;

  systemOS:String;

  systemVersion:String;

  systemDevice:String;

  shabaCode:string;

  nationalCode:string;

  postalCode:string;

  address:string;

  firstName:string;

  lastName:string;

  @Type(() => UploadedFile)
  profileImage:UploadedFile;

  @Type(() => UploadedFile)
  nationalCardImage:UploadedFile;

}
