

import {Category} from "./Category";
import {UploadedFile} from "./UploadedFile";
import {Tag} from "./Tag";
import {Type} from "class-transformer/index";
import {University} from "./University";
import {User} from "./User";


export class Content {
  id:number;
  uid:string;

  title:string;
  constructor()
  {
    this.files = [];
    this.images = [];
    this.tags = [];
  }

  @Type(() => UploadedFile)
  coverImage:UploadedFile;


  @Type(() => Date)
  creationDate:Date;

  @Type(() => Date)
  publishedDate:Date;

  @Type(() => Date)
  archivedDate:Date;

  @Type(() => Date)
  editedDate:Date;

  productState:ProductState;

  @Type(() => Category)
  category:Category;

  @Type(() => Category)
  optionalCategories:Category[];

  @Type(() => University)
  university:University;

  @Type(() => UploadedFile)
   files:UploadedFile[];

  @Type(() => UploadedFile)
   images:UploadedFile[];

  @Type(() => Comment)
   comments:Comment[];



  professor:string;

  @Type(() => User)
  createdBy:User;

  writeYear:string;

  @Type(() => Tag)
  tags:Tag[];

  description:string;

  writer:string;
  pagesNum:number;

  price:number;

  avgRate:number;

  downloadsNum:number;
  contentType:ContentType;


}
export enum ProductState {Evaluating, Published, Canceled, NeedChange, Archived}
export enum PersianProductState {'در حال بررسی', 'منتشر شده', 'نیاز به تغییر', 'لغو شده', 'آرشیو شده'}
export enum ContentType {
  Booklet, SampleQuestion, Report, Article, Project
}
