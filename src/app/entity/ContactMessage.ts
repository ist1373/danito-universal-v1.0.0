
import {User} from "./User";
import {Product} from "./Product";
import {Type} from "class-transformer/index";
export class ContactMessage {

  id:Number;
  email:string;
  content:string;
  @Type(() => User)
  createdBy:User;

  deviceId:string;


  @Type(() => Date)
  creationDate:Date;


}

