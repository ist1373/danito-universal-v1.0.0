import {User} from "./User";
import {UploadedFile} from "./UploadedFile";
import {Type} from "class-transformer/index";
/**
 * Created by iman on 20/02/2017.
 */
export class Product {

  id:number;
  uid:string;

  title:String;

  @Type(() => UploadedFile)
  coverImage:UploadedFile;

  @Type(() => User)
  createdBy:User;

  @Type(() => Date)
  creationDate:Date;

  @Type(() => Date)
  publishedDate:Date;

  @Type(() => Date)
  archivedDate:Date;

  @Type(() => Date)
  editedDate:Date;

  productState:ProductState;

}

 enum ProductState {Evaluating, Published, Canceled, NeedChange, Archived}
