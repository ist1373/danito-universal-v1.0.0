import {Type} from "class-transformer/index";
import {Product} from "./Product";
import {User} from "./User";
/**
 * Created by iman on 16/02/2017.
 */

export class Transaction {


  id:number;
  amount:number;
  refId:string;
  authority:string;
  errorCode:number;

  @Type(() => Date)
  creationDate:Date;

  @Type(() => Product)
  product:Product;

  @Type(() => User)
  payer:User;

  paymentState:PaymentState;

  paymentType:PaymentType;
}

 enum PaymentState {Done, Cancel, Error, Pending, Fraud}

 enum PaymentType {Content}
