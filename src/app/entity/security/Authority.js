/**
 * Created by iman on 5/22/2017.
 */
var Authority = (function () {
    function Authority() {
    }
    return Authority;
}());
export { Authority };
var AuthorityName;
(function (AuthorityName) {
    AuthorityName[AuthorityName["ROLE_USER"] = 0] = "ROLE_USER";
    AuthorityName[AuthorityName["ROLE_ADMIN"] = 1] = "ROLE_ADMIN";
})(AuthorityName || (AuthorityName = {}));
//# sourceMappingURL=Authority.js.map