
import {Book} from './Book';
import {Type} from 'class-transformer/index';
import {UploadedFile} from './UploadedFile';
import {User} from './User';
import {City} from './City';
import {University} from './University';

export class  BookPlace{

  id:number;

  title:string;


  description:string;


  address:string;

  phoneNumber:string;

  @Type(() => UploadedFile)
  coverImage:UploadedFile;

  @Type(() => User)
   createdBy:User;

  @Type(() => City)
  city:City

  @Type(() => University)
  university:University;

  placeState:PlaceState;

}


export   enum PlaceState {Evaluating, Activated, Disabled, NeedChange, Rejected}
