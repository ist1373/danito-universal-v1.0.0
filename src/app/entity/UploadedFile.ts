/**
 * Created by iman on 10/02/2017.
 */
import {Type} from "class-transformer/index";
import {User} from "./User";
/**
 * Created by iman on 10/02/2017.
 */

export class UploadedFile {

    id:number;

    name:String;

    size:number;

    originalPath:String;

    thumbnailPath:String;

    description:String;

    @Type(() => Date)
    creationDate:Date;

    fileType:String;

    @Type(() => User)
    createdBy:User;


    status:number = 400;

    accessType:AccessType;
}

export enum AccessType {Public, Private, AccessOnPay,Forbidden}
