import {User} from "./User";
import {UploadedFile} from "./UploadedFile";
import {Category} from "./Category";
import {Product} from "./Product";
import {Type} from "class-transformer/index";
import {SafeUrl} from "@angular/platform-browser/public_api";
/**
 * Created by iman on 16/02/2017.
 */

export class Slider {



  id:number;

  @Type(() => User)
  user:User;

  @Type(() => Date)
  startDate:Date;

  @Type(() => Date)
  endDate:Date;

  @Type(() => UploadedFile)
  image:UploadedFile;

  @Type(() => Category)
  category:Category;

  @Type(() => Product)
  product:Product;

  imageUrl:SafeUrl;

  viewUrl:String;


  viewAction:String;


  position:number;


  sliderType:SliderType;

}

enum SliderType {
  Book, Content, Category, Poster
}
