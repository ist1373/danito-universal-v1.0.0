import {Component, Inject, PLATFORM_ID} from '@angular/core';
import {BooksumCacheService} from './service/booksum-cache.service';
import {CacheService} from 'ng2-cache-service';
import {UserService} from './service/user.service';
import {UniversityService} from './service/university.service';
import {HttpInterceptorService} from 'ng-http-interceptor';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';

import {isPlatformBrowser, isPlatformServer} from '@angular/common';
// import {CacheStoragesEnum} from "ng2-cache-service/dist/src/enums/cache-storages.enum";

// declare var init:any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [BooksumCacheService, CacheService, UserService, UniversityService]
})
export class AppComponent {
  title = 'app works!';
  constructor(@Inject(PLATFORM_ID) private platformId: Object, private booksumCacheService: BooksumCacheService, private cacheService: CacheService, httpInterceptor: HttpInterceptorService, private slimLoadingBarService: SlimLoadingBarService) {
    // this.cacheService.useStorage(CacheStoragesEnum.LOCAL_STORAGE);
    // init();
    this.cacheService.set('platform',this.platformId);
    if (isPlatformBrowser(this.cacheService.get('platform'))) {
      // Client only code.
    }
    if (isPlatformServer(this.platformId)) {
      // Server only code.
    }


    httpInterceptor.request().addInterceptor((data, method) => {
      // console.log(method, data);
      slimLoadingBarService.start();

      return data;

    });

    httpInterceptor.response().addInterceptor((res, method) => {
      return res.do(r => {
        slimLoadingBarService.complete();
      });
    });
  }
  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {
      window.scrollTo(0, 0);
    }
    // this.booksumCacheService.loadCaches();
  }



}
