import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {MdDialogRef} from "@angular/material";
import {Content} from "../entity/Content";
import {BookletService} from "../service/booklet.service";
import {CacheService} from "ng2-cache-service";
import {isPlatformBrowser} from "@angular/common";

@Component({
  selector: 'app-factor-dialog',
  templateUrl: './factor-dialog.component.html',
  styleUrls: ['./factor-dialog.component.scss'],
  providers:[BookletService,CacheService]
})
export class FactorDialogComponent implements OnInit {

  content:Content;
  connectionCode = 0;
  constructor(private cacheService:CacheService,private router: Router, public dialogRef: MdDialogRef<FactorDialogComponent>,private bookletService:BookletService) { }

  ngOnInit() {
    if (isPlatformBrowser(this.cacheService.get('platform'))) {
      this.bookletService.connectToZarinpalGateway(this.content.price,window.location.href,this.content.title.toString()).subscribe((res)=>{
        // window.location.href = 'https://www.zarinpal.com/pg/StartPay/' + res.json()['message'];
        this.connectionCode = res.json()['message'];


        // var newWindow = window.open(
        //   'https://www.zarinpal.com/pg/StartPay/' + res.json()['message'],
        //   '_blank' // <- This is what makes it open in a new window.
        // );
      });
    }
  }


}
