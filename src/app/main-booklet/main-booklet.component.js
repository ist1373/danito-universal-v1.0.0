var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { MainBookletService } from "./main-booklet.service";
// import {JsonObject, JsonProperty,JsonConvert} from "json2typescript";
import { plainToClass } from "class-transformer";
import { ProductMarket } from "../entity/model/ProductMarket";
import { SliderSection } from "../entity/model/SliderSection";
import { WebComponent, WebComponentType } from "./WebComponent";
import { ActivatedRoute } from "@angular/router";
import { SlimLoadingBarService } from "ng2-slim-loading-bar";
// var jalaali = require('jalaali-js')
var MainBookletComponent = (function () {
    function MainBookletComponent(mainBooklet, activatedRoute, slimLoadingBarService) {
        this.mainBooklet = mainBooklet;
        this.activatedRoute = activatedRoute;
        this.slimLoadingBarService = slimLoadingBarService;
        this.webComponents = [];
        this.jozzveSectionType = WebComponentType.JozzveSection;
        this.sliderSectionType = WebComponentType.SliderSection;
        this.categorySectionType = WebComponentType.CategorySection;
        this.posterSectionType = WebComponentType.PosterSection;
        this.specialJozzveType = WebComponentType.SpecialJozzve;
    }
    // @Output() onLoadSlider:EventEmitter<SliderSection>;
    MainBookletComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.mainBooklet.getBookletMarket().subscribe(function (data) {
            console.log(data.json());
            var bookletmarket = plainToClass(ProductMarket, data.json());
            console.log(bookletmarket);
            _this.loadSlider(bookletmarket.sliderSection);
            _this.loadJozzveSection(bookletmarket.jozveSections);
            _this.loadCategorySection(bookletmarket.categorySections);
            _this.loadSpecialJozzve(bookletmarket.specialJozves);
            _this.loadPosterSection(bookletmarket.posterSections);
            console.log(bookletmarket.categorySections[0].categoryList[0].title);
            // this.slimLoadingBarService.complete();
        });
        // this.activatedRoute.snapshot.queryParams.subscribe((params: Params) => {
        //
        //   console.log(params["Authority"]+"dfgdfgdfgdfg");
        //   // this.bookletService.verifyZarinPayment(this.booklet.price,UserService.user.id,this.booklet.id,params["Authority"]).subscribe((res)=>{
        //   //     console.log("transaction was successful");
        //   //     console.log(res);
        //   // });
        // });
        // console.log(this.activatedRoute.snapshot.queryParams["Authority"].toString());
    };
    MainBookletComponent.prototype.loadSlider = function (sliderSection) {
        // this.sliderComponent.initSlider(sliderSection);
        var webcomp = new WebComponent();
        webcomp.componentType = WebComponentType.SliderSection;
        webcomp.sliderSection = sliderSection;
        this.webComponents[sliderSection.position] = webcomp;
    };
    MainBookletComponent.prototype.loadJozzveSection = function (jozzveSections) {
        for (var _i = 0, jozzveSections_1 = jozzveSections; _i < jozzveSections_1.length; _i++) {
            var js = jozzveSections_1[_i];
            var webcomp = new WebComponent();
            webcomp.componentType = WebComponentType.JozzveSection;
            webcomp.jozzveSection = js;
            this.webComponents[js.position] = webcomp;
        }
    };
    MainBookletComponent.prototype.loadCategorySection = function (categorySections) {
        for (var _i = 0, categorySections_1 = categorySections; _i < categorySections_1.length; _i++) {
            var cs = categorySections_1[_i];
            var webcomp = new WebComponent();
            webcomp.componentType = WebComponentType.CategorySection;
            webcomp.categorySection = cs;
            this.webComponents[cs.position] = webcomp;
        }
    };
    MainBookletComponent.prototype.loadSpecialJozzve = function (specialJozzve) {
        for (var _i = 0, specialJozzve_1 = specialJozzve; _i < specialJozzve_1.length; _i++) {
            var sj = specialJozzve_1[_i];
            var webcomp = new WebComponent();
            webcomp.componentType = WebComponentType.SpecialJozzve;
            webcomp.specialJozzve = sj;
            this.webComponents[sj.position] = webcomp;
        }
    };
    MainBookletComponent.prototype.loadPosterSection = function (posterSection) {
        for (var _i = 0, posterSection_1 = posterSection; _i < posterSection_1.length; _i++) {
            var ps = posterSection_1[_i];
            var webcomp = new WebComponent();
            webcomp.componentType = WebComponentType.PosterSection;
            webcomp.posterSection = ps;
            this.webComponents[ps.position] = webcomp;
        }
    };
    return MainBookletComponent;
}());
MainBookletComponent = __decorate([
    Component({
        selector: 'app-main-booklet',
        templateUrl: './main-booklet.component.html',
        styleUrls: ['./main-booklet.component.scss'],
        providers: [MainBookletService, SliderSection]
    }),
    __metadata("design:paramtypes", [MainBookletService, ActivatedRoute, SlimLoadingBarService])
], MainBookletComponent);
export { MainBookletComponent };
//# sourceMappingURL=main-booklet.component.js.map
