import {Component, HostListener, OnInit, Output, ViewChild} from '@angular/core';
import {AppContextUrl, FileContextUrl} from "../globals";
import {BookletService} from "../service/booklet.service";
import {Response} from "@angular/http";
import {Book} from "../entity/Book";
// import {JsonObject, JsonProperty,JsonConvert} from "json2typescript";
import {plainToClass} from "class-transformer";
import {Observable} from "rxjs/Rx";
import {ProductMarket} from "../entity/model/ProductMarket";
import {Content} from "../entity/Content";
import {SliderSection} from "../entity/model/SliderSection";
import {SliderComponent} from "../slider/slider.component";
import {ContentSection} from "../entity/model/ContentSection";
import {WebComponent, WebComponentType} from "../entity/model/WebComponent";
import {CategorySection} from "../entity/model/CategorySection";
import {SpecialContent} from "../entity/model/SpecialContent";
import {PosterSection} from "../entity/model/PosterSection";

import * as jalaali from 'jalaali-js';
import {forEach} from "@angular/router/src/utils/collection";
import {Category} from "../entity/Category";
import {ActivatedRoute, Params} from "@angular/router";
import {SlimLoadingBarService} from "ng2-slim-loading-bar";
import {MetaService} from "@ngx-meta/core";
import {isPlatformBrowser} from "@angular/common";
import {CacheService} from "ng2-cache-service";
// var jalaali = require('jalaali-js')


@Component({
  selector: 'app-main-booklet',
  templateUrl: './main-booklet.component.html',
  styleUrls: ['./main-booklet.component.scss'],
  providers:[BookletService,SliderSection,CacheService]
})
export class MainBookletComponent implements OnInit {


  webComponents :WebComponent[] = [];
  jozzveSectionType = WebComponentType.JozzveSection;
  sliderSectionType = WebComponentType.SliderSection;
  categorySectionType = WebComponentType.CategorySection;
  posterSectionType = WebComponentType.PosterSection;
  specialJozzveType = WebComponentType.SpecialJozzve;


  constructor(private mainBooklet:BookletService, private activatedRoute:ActivatedRoute,
              private cacheService:CacheService,private slimLoadingBarService: SlimLoadingBarService,private readonly metaService: MetaService) {
  }
  // @Output() onLoadSlider:EventEmitter<SliderSection>;



  ngOnInit() {
    this.metaService.setTitle( ' دانیتو مرجع جزوات خلاصه کتاب حل تمارین و تمام محصولات دانشگاهی danito.ir ',true);
    this.metaService.setTag( 'og:image',FileContextUrl + 'fid=2498'+ '&isThumb=true');
    this.metaService.setTag('og:description','بزرگترین مرجع جزوات خلاصه کتاب حل تمارین و تمام محصولات دانشگاهی');
    this.metaService.setTag('og:locale','fa_IR');
    this.metaService.setTag('og:image:width','200px');
    this.metaService.setTag('og:image:height','200px');


    if (isPlatformBrowser(this.cacheService.get('platform'))) {
      window.scrollTo(0, 0);
    }
    this.mainBooklet.getContentMarket().subscribe((data:Response)=>{

       console.log(data.json());
      var bookletmarket = plainToClass<ProductMarket, Object>(ProductMarket, data.json());
      console.log(bookletmarket);
      this.loadSlider(bookletmarket.sliderSection);
      this.loadJozzveSection(bookletmarket.contentSections);
      this.loadCategorySection(bookletmarket.categorySections);
      this.loadSpecialJozzve(bookletmarket.specialContents);
      this.loadPosterSection(bookletmarket.posterSections);




      // this.slimLoadingBarService.complete();
    });
    // this.activatedRoute.snapshot.queryParams.subscribe((params: Params) => {
    //
    //   console.log(params["Authority"]+"dfgdfgdfgdfg");
    //   // this.bookletService.verifyZarinPayment(this.booklet.price,UserService.user.id,this.booklet.id,params["Authority"]).subscribe((res)=>{
    //   //     console.log("transaction was successful");
    //   //     console.log(res);
    //   // });
    // });
    // console.log(this.activatedRoute.snapshot.queryParams["Authority"].toString());


  }



  loadSlider(sliderSection:SliderSection)
  {
    // this.sliderComponent.initSlider(sliderSection);
    var webcomp = new WebComponent();
    webcomp.componentType = WebComponentType.SliderSection;
    webcomp.sliderSection = sliderSection;
    this.webComponents[sliderSection.position] = webcomp;

  }


  loadJozzveSection(jozzveSections:ContentSection[]){
    for(let js of jozzveSections)
    {
      var webcomp = new WebComponent();
      webcomp.componentType = WebComponentType.JozzveSection;
      webcomp.jozzveSection = js;
      this.webComponents[js.position] = webcomp;
    }
  }

  loadCategorySection(categorySections:CategorySection[]){
    for(let cs of categorySections)
    {
      var webcomp = new WebComponent();
      webcomp.componentType = WebComponentType.CategorySection;
      webcomp.categorySection = cs;
      this.webComponents[cs.position] =webcomp;
    }
  }

  loadSpecialJozzve(specialJozzve:SpecialContent[]){
    for(let sj of specialJozzve)
    {
      var webcomp = new WebComponent();
      webcomp.componentType = WebComponentType.SpecialJozzve;
      webcomp.specialJozzve = sj;
      this.webComponents[sj.position] =webcomp;
    }
  }

  loadPosterSection(posterSection:PosterSection[]){
    for(let ps of posterSection)
    {
      var webcomp = new WebComponent();
      webcomp.componentType = WebComponentType.PosterSection;
      webcomp.posterSection = ps;
      this.webComponents[ps.position] =webcomp;
    }
  }

  // @HostListener('window:scroll', ['$event'])
  //  navHeader(event) {
  //
  //   if (window.pageYOffset >= 100) {
  //     this.bigHeader = false;
  //   } else if (window.pageYOffset < 100) {
  //     this.bigHeader = true;
  //   }
  //
  // }


}
