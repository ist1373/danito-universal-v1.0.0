import {Component, HostListener, Inject, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {BookletService} from "../../service/booklet.service";
import {Content} from "../../entity/Content";
import {CacheService} from "ng2-cache-service";
import {ItemSizePerPage} from "../../globals";
import {DOCUMENT} from "@angular/platform-browser";
import {FormControl, FormGroup} from "@angular/forms";
import {CategoryService} from "../../category-dialog/category-service";
import {MdDialog} from "@angular/material";
import {CategoryDialogComponent} from "../../category-dialog/category-dialog.component";
import {plainToClass} from "class-transformer";
import {Category} from "../../entity/Category";
import {Http,Response} from "@angular/http";
import {University} from "../../entity/University";
import {UniversityService} from "../../service/university.service";
import {tick} from "@angular/core/testing";
import {isPlatformBrowser} from "@angular/common";

@Component({
  selector: 'app-booklet-list',
  templateUrl: './booklet-list.component.html',
  styleUrls: ['./booklet-list.component.scss'],
  providers:[BookletService,CacheService,CategoryService,UniversityService]
})
export class BookletListComponent implements OnInit {

  booklets:Content[];
  listFromUrl = "";
  catid=0;
  tagid = 0;
  title="";
  showMore = false;
  type= "";
  page = 0;
  universities : Array<any>=[];
  searchForm:FormGroup;
  titleSearch = new FormControl('');
  professorSearch = new FormControl('');
  writeYearSearch = new FormControl('');
  categorySearch:Category;
  universitySearch:University;

  scrollOffsetToLoad = 300;

  constructor(private activatedRoute:ActivatedRoute,private bookletService:BookletService,private universityService:UniversityService,
              private cacheService:CacheService,private categoryService:CategoryService,private dialog:MdDialog) {
      this.searchForm = new FormGroup({
        title: this.titleSearch,
        professor:this.professorSearch,
        writeYear:this.writeYearSearch
      });
      this.categorySearch = new Category();
      this.universitySearch = new University();
  }

  ngOnInit() {
    if (isPlatformBrowser(this.cacheService.get('platform'))) {
      window.scrollTo(0, 0);
    }
    console.log(this.cacheService.get("source"));
    this.activatedRoute.url.subscribe((url)=>{
      this.page = 0 ;
      this.scrollOffsetToLoad = 300;
      console.log('changeeeeeeeeeeeee');

      if(url[1].toString()=="list")
      {
        this.type = "list";
        var temp = this.cacheService.get("listFromUrl").toString();
        this.listFromUrl = temp.substring(0,temp.length-2);
        this.bookletService.getContentsByURL(this.listFromUrl+this.page+'&size='+ItemSizePerPage).subscribe((booklets)=>{
          this.booklets = booklets;
          if(this.booklets.length==ItemSizePerPage)
          {
            this.showMore =true;
          }
        });
        this.title = this.cacheService.get("title").toString();
      }
      else if(url[1].toString()=="cat") {
        this.type = "cat";
        this.activatedRoute.params.subscribe((params: Params) => {
          this.catid = params['id'];
          this.title = params['name'];
          this.loadByCat(this.catid,this.page,false);
        });
      }
      else if(url[1].toString()=="tag") {
        this.type = "tag";
        this.activatedRoute.params.subscribe((params: Params) => {
          this.tagid = params['id'];
          this.loadByTag(this.tagid,this.page,false);
        });
      }
      else if(url[1].toString()=="search") {
        this.type = "search";
        this.initUniversirsities();
        // this.scrollOffsetToLoad = 100;
      }


    });
  }

  ngAfterViewInit(){
    CategoryService.categoryObserver.subscribe((cat:Category)=>{
      this.categorySearch = cat;
    });
  }


  loadByCat(catId:number,page:number,more:boolean)
  {
    console.log('cat:::'+ page);
    this.bookletService.getContentsByCategory(catId,page).subscribe((contents)=>{
      if(!more)
      {
        this.booklets = contents;
      }
      else
      {
        for(let content of contents)
        {
          this.booklets.push(content);
        }
      }
      if(this.booklets.length==ItemSizePerPage)
      {
        this.showMore =true;
      }
    });
  }
  loadByTag(tagId:number,page:number,more:boolean)
  {
    this.bookletService.getContentsByTag(tagId,this.page).subscribe((contents)=>{
      if(!more)
      {
        this.booklets = contents;
      }
      else
      {
        for(let content of contents)
        {
          this.booklets.push(content);
        }
      }
        if(this.booklets.length==ItemSizePerPage)
        {
          this.showMore =true;
        }
    });
  }


  @HostListener('window:scroll', ['$event'])
  doSomething(event) {
    // console.log(window.pageYOffset)
    if (isPlatformBrowser(this.cacheService.get('platform'))) {
      if (this.scrollOffsetToLoad <= window.pageYOffset) {
        this.loadMore();
        this.scrollOffsetToLoad += 700;
        console.log(this.scrollOffsetToLoad);
      }
    }
  }

  loadMore(){
    if(this.type == "list")
    {
      this.page++;
      this.bookletService.getContentsByURL(this.listFromUrl+this.page+'&size='+ItemSizePerPage).subscribe((booklets:Content[])=>{
        for(let jozve of booklets)
        {
          this.booklets.push(jozve);
        }
        if(booklets.length==0)
        {
          this.showMore =false;
        }
      });
    }
    else if(this.type == "cat")
    {
      this.page++;
      this.loadByCat(this.catid,this.page,true);
    }
    else if(this.type == "tag")
    {
      this.page++;
      this.loadByTag(this.tagid,this.page,true);
    }
    else if(this.type == "search")
    {
      this.page++;
      this.findContent(this.page);
    }

  }

  openCategoryModal(){
    this.categoryService.getParentsCategory().subscribe((data:Response)=> {
      let dialogRef = this.dialog.open(CategoryDialogComponent);
      dialogRef.componentInstance.returnCatId = true;
      dialogRef.componentInstance.categories = plainToClass(Category, data.json());
      dialogRef.componentInstance.categoryHeader ="دسته بندی";
    });
  }

  initUniversirsities()
  {
    this.universityService.getUniversities().subscribe((unis:University[])=>{
      for(let uni of unis)
      {
        this.universities.push({value:uni.id,label:uni.title.toString()});
      }
    });
    this.cacheService.set("unis",this.universities);
  }

  public selectUniversity(item:any):void {
    let uni = new University();
    this.universitySearch.id = item.value;
  }

  findContent(page:number){
    if(page == 0) this.page = 0;
    this.bookletService.findContent(this.titleSearch.value,this.professorSearch.value,this.writeYearSearch.value,this.universitySearch.id,this.categorySearch.id,page)
      .subscribe((contents)=>{
        if(page == 0)
          this.booklets = contents;
        else {
          for(let con of contents)
          {
            this.booklets.push(con);
          }
        }
      });
  }
}
