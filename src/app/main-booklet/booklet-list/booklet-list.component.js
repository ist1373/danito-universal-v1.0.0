var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { MainBookletService } from "../main-booklet.service";
import { CacheService } from "ng2-cache-service";
import { ItemSizePerPage } from "../../globals";
var BookletListComponent = (function () {
    function BookletListComponent(activatedRoute, bookletService, cacheService) {
        this.activatedRoute = activatedRoute;
        this.bookletService = bookletService;
        this.cacheService = cacheService;
        this.listFromUrl = "";
        this.catid = 0;
        this.title = "";
        this.showMore = false;
        this.type = "";
        this.page = 0;
        // console.log(this.route.snapshot.params['url']);
    }
    BookletListComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log(this.cacheService.get("source"));
        this.activatedRoute.url.subscribe(function (url) {
            if (url[1].toString() == "list") {
                _this.type = "list";
                var temp = _this.cacheService.get("listFromUrl").toString();
                _this.listFromUrl = temp.substring(0, temp.length - 2);
                _this.bookletService.getBookletsByURL(_this.listFromUrl + _this.page + '&size=' + ItemSizePerPage).subscribe(function (booklets) {
                    _this.booklets = booklets;
                    if (_this.booklets.length == ItemSizePerPage) {
                        _this.showMore = true;
                    }
                });
                _this.title = _this.cacheService.get("title").toString();
                // MainBookletService.tempVar.delete("listFromUrl");
            }
            else if (url[1].toString() == "cat") {
                _this.type = "cat";
                _this.activatedRoute.params.subscribe(function (params) {
                    _this.catid = params['id'];
                    _this.title = params['name'];
                    _this.loadByCat(_this.catid, _this.page);
                });
                // var catId:number = this.cacheService.get("catId");
                // this.title = this.cacheService.get("title").toString();
            }
        });
    };
    BookletListComponent.prototype.loadByCat = function (catId, page) {
        var _this = this;
        this.bookletService.getBookletsByCategory(catId, 0).subscribe(function (booklets) {
            _this.booklets = booklets;
            if (_this.booklets.length == ItemSizePerPage) {
                _this.showMore = true;
            }
        });
    };
    // @HostListener('window:scroll', ['$event']) onScrollEvent($event){
    //   // console.log($event);
    //   console.log(this.document.body.scrollHeight);
    //   let number = this.document.body.scrollHeight;
    //   if (number > 100) {
    //     // this.navIsFixed = true;
    //     console.log("fixxxx");
    //   }
    // }
    BookletListComponent.prototype.loadMore = function () {
        var _this = this;
        if (this.type == "list") {
            this.page++;
            this.bookletService.getBookletsByURL(this.listFromUrl + this.page + '&size=' + ItemSizePerPage).subscribe(function (booklets) {
                for (var _i = 0, booklets_1 = booklets; _i < booklets_1.length; _i++) {
                    var jozve = booklets_1[_i];
                    _this.booklets.push(jozve);
                }
                if (booklets.length == 0) {
                    _this.showMore = false;
                }
            });
        }
        else if (this.type == "cat") {
            this.page++;
            this.loadByCat(this.catid, this.page);
        }
    };
    return BookletListComponent;
}());
BookletListComponent = __decorate([
    Component({
        selector: 'app-booklet-list',
        templateUrl: './booklet-list.component.html',
        styleUrls: ['./booklet-list.component.scss'],
        providers: [MainBookletService, CacheService]
    }),
    __metadata("design:paramtypes", [ActivatedRoute, MainBookletService,
        CacheService])
], BookletListComponent);
export { BookletListComponent };
//# sourceMappingURL=booklet-list.component.js.map
