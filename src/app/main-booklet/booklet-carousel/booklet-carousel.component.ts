import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {ContentSection} from "../../entity/model/ContentSection";
import {ActivatedRoute, Router} from "@angular/router";
import {BookletPage} from "./BookletPage";
import {BookletService} from "../../service/booklet.service";
import {CacheService} from "ng2-cache-service";
import {FileContextUrl} from "../../globals";
import {OwlCarousel} from "ngx-owl-carousel";

@Component({
  selector: 'app-booklet-carousel',
  templateUrl: './booklet-carousel.component.html',
  styleUrls: ['./booklet-carousel.component.scss'],
  providers:[BookletService,CacheService]
})
export class BookletCarouselComponent implements OnInit {
  @Input() car_id:string ;
  @Input() jozzveSection:ContentSection ;
  bookletPages:Array<BookletPage>;
  // @ViewChild('owlElement') owlElement: OwlCarousel;


  constructor(private cacheService :CacheService,private router:Router) {
    this.bookletPages = new Array<BookletPage>();

  }

  ngOnInit() {
    this.initBookletPages(this.jozzveSection)
  }


  loadMore()
  {
    this.cacheService.set("source", "fromUrl");
    this.cacheService.set("listFromUrl", this.jozzveSection.loadMoreLink.toString());
    let biutifyTitle = this.jozzveSection.title.replace(/ /g,'-');
    this.cacheService.set("title", this.jozzveSection.title);
    this.router.navigate(['/content/list',biutifyTitle]);
  }


  initBookletPages(jozzveSection:ContentSection)
  {
    for (let i = 0;i<jozzveSection.contentList.length;i+=4)
    {
      let bookletPage= new BookletPage();
      for(let j = i;j<i+4;j++)
      {
        bookletPage.jozves.push(jozzveSection.contentList[j]);

      }
      this.bookletPages.push(bookletPage);
    }
  }

}
