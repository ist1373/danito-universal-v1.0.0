var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { JozveSection } from "../../entity/model/JozveSection";
import { ActivatedRoute, Router } from "@angular/router";
import { BookletPage } from "./BookletPage";
import { MainBookletService } from "../main-booklet.service";
import { CacheService } from "ng2-cache-service";
var BookletCarouselComponent = (function () {
    function BookletCarouselComponent(cacheService, router, aRoute) {
        this.cacheService = cacheService;
        this.router = router;
        this.aRoute = aRoute;
        this.bookletPages = new Array();
    }
    BookletCarouselComponent.prototype.ngOnInit = function () {
        this.initBookletPages(this.jozzveSection);
    };
    BookletCarouselComponent.prototype.loadMore = function () {
        // console.log("ld");
        // this.aRoute.snapshot.data['url'] = this.jozzveSection.loadMoreLink;
        // MainBookletService.tempVar.set("fromUrl","") ;
        this.cacheService.set("source", "fromUrl");
        this.cacheService.set("listFromUrl", this.jozzveSection.loadMoreLink.toString());
        this.cacheService.set("title", this.jozzveSection.title);
        this.router.navigate(['/booklet/list', this.jozzveSection.title]);
    };
    BookletCarouselComponent.prototype.initBookletPages = function (jozzveSection) {
        for (var i = 0; i < jozzveSection.jozveList.length; i += 4) {
            var bookletPage = new BookletPage();
            for (var j = i; j < i + 4; j++) {
                bookletPage.jozves.push(jozzveSection.jozveList[j]);
            }
            this.bookletPages.push(bookletPage);
        }
    };
    return BookletCarouselComponent;
}());
__decorate([
    Input(),
    __metadata("design:type", String)
], BookletCarouselComponent.prototype, "car_id", void 0);
__decorate([
    Input(),
    __metadata("design:type", JozveSection)
], BookletCarouselComponent.prototype, "jozzveSection", void 0);
BookletCarouselComponent = __decorate([
    Component({
        selector: 'app-booklet-carousel',
        templateUrl: './booklet-carousel.component.html',
        styleUrls: ['./booklet-carousel.component.scss'],
        providers: [MainBookletService, CacheService]
    }),
    __metadata("design:paramtypes", [CacheService, Router, ActivatedRoute])
], BookletCarouselComponent);
export { BookletCarouselComponent };
//# sourceMappingURL=booklet-carousel.component.js.map
