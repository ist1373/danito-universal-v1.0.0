var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { Jozve } from "../../entity/Jozve";
import { trigger, state, style, transition, animate } from '@angular/core';
var BookletComponent = (function () {
    function BookletComponent() {
        this.state = 'inactive';
        this.booklet = new Jozve();
    }
    BookletComponent.prototype.toggleMove = function () {
        this.state = (this.state === 'inactive' ? 'active' : 'inactive');
    };
    BookletComponent.prototype.ngOnInit = function () {
    };
    return BookletComponent;
}());
__decorate([
    Input(),
    __metadata("design:type", Jozve)
], BookletComponent.prototype, "booklet", void 0);
BookletComponent = __decorate([
    Component({
        selector: 'app-booklet',
        templateUrl: './booklet.component.html',
        styleUrls: ['./booklet.component.scss'],
        providers: [],
        animations: [
            trigger('focusPanel', [
                state('inactive', style({
                    transform: 'scale(1)',
                    backgroundColor: '#eee'
                })),
                state('active', style({
                    transform: 'scale(1.1)',
                    backgroundColor: '#cfd8dc'
                })),
                transition('inactive => active', animate('100ms ease-in')),
                transition('active => inactive', animate('100ms ease-out'))
            ]),
        ]
    }),
    __metadata("design:paramtypes", [])
], BookletComponent);
export { BookletComponent };
//# sourceMappingURL=booklet.component.js.map
