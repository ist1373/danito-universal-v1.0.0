import {Component, Input, OnInit} from '@angular/core';
import {Content} from "../../entity/Content";
import {MdTooltipModule} from "@angular/material";

import {
  trigger,
  state,
  style,
  transition,
  animate,
  keyframes
} from '@angular/core';
import {BooksumCacheService} from "../../service/booksum-cache.service";




@Component({
  selector: 'app-booklet',
  templateUrl: './booklet.component.html',
  styleUrls: ['./booklet.component.scss'],
  providers:[],
  animations: [

    trigger('focusPanel', [
      state('inactive', style({
        transform: 'scale(1)',
        backgroundColor: '#eee'
      })),
      state('active', style({
        transform: 'scale(1.1)',
        backgroundColor: '#cfd8dc'
      })),
      transition('inactive => active', animate('100ms ease-in')),
      transition('active => inactive', animate('100ms ease-out'))
    ]),

  ]
})
export class BookletComponent implements OnInit {
  @Input() booklet:Content;
  @Input() forCarousel = false;
  beautifyTitle = '';

  constructor() {
    this.booklet = new Content();

  }

  state: string = 'inactive';

  toggleMove() {
    this.state = (this.state === 'inactive' ? 'active' : 'inactive');
  }

  ngOnInit() {
    if(this.booklet!=null)
    this.beautifyTitle = this.booklet.title.replace(/ /g,'-');
    // this.beautifyTitle =  this.beautifyTitle.replace('20','-');
    // console.log(this.booklet);
  }

}
