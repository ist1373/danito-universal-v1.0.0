import {Component, Input, OnInit} from '@angular/core';
import {ContentSection} from "../../entity/model/ContentSection";
import {CategoryPage} from "./CategoryPage";
import {CategorySection} from "../../entity/model/CategorySection";
import {Category} from "../../entity/Category";

@Component({
  selector: 'app-category-section',
  templateUrl: './category-section.component.html',
  styleUrls: ['./category-section.component.scss']
})
export class CategorySectionComponent implements OnInit {
  categoryPage: any;

  constructor() {
    this.categoryPages = new Array<CategoryPage>();
  }

  @Input() car_id:string ;
  @Input() categorySection:CategorySection ;
  categoryPages:Array<CategoryPage>;

  ngOnInit() {
    this.initCategoryPages(this.categorySection)
  }


  initCategoryPages(categorySection:CategorySection)
  {
    for (let i = 0;i<categorySection.categoryList.length;i+=3)
    {
      let categoryPage= new CategoryPage();
      for(let j = i;j<i+3;j++)
      {
        categoryPage.categories.push(categorySection.categoryList[j]);
      }
      this.categoryPages.push(categoryPage);
    }
  }

}
