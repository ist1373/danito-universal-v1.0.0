import {Component, Input, OnInit} from '@angular/core';
import {Category} from "../../../entity/Category";
import {AppContextUrl, FileContextUrl} from "../../../globals";

@Component({
  selector: 'app-category-item',
  templateUrl: './category-item.component.html',
  styleUrls: ['./category-item.component.scss']
})
export class CategoryItemComponent implements OnInit {
  @Input() category:Category ;
  base=AppContextUrl;
  baseFile = FileContextUrl;

  constructor() { }

  ngOnInit() {
  }

}
