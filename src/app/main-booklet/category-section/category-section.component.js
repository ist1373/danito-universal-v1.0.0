var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { CategoryPage } from "./CategoryPage";
import { CategorySection } from "../../entity/model/CategorySection";
var CategorySectionComponent = (function () {
    function CategorySectionComponent() {
        this.categoryPages = new Array();
    }
    CategorySectionComponent.prototype.ngOnInit = function () {
        this.initCategoryPages(this.categorySection);
    };
    CategorySectionComponent.prototype.initCategoryPages = function (categorySection) {
        for (var i = 0; i < categorySection.categoryList.length; i += 3) {
            var categoryPage = new CategoryPage();
            for (var j = i; j < i + 3; j++) {
                categoryPage.categories.push(categorySection.categoryList[j]);
            }
            this.categoryPages.push(categoryPage);
        }
    };
    return CategorySectionComponent;
}());
__decorate([
    Input(),
    __metadata("design:type", String)
], CategorySectionComponent.prototype, "car_id", void 0);
__decorate([
    Input(),
    __metadata("design:type", CategorySection)
], CategorySectionComponent.prototype, "categorySection", void 0);
CategorySectionComponent = __decorate([
    Component({
        selector: 'app-category-section',
        templateUrl: './category-section.component.html',
        styleUrls: ['./category-section.component.scss']
    }),
    __metadata("design:paramtypes", [])
], CategorySectionComponent);
export { CategorySectionComponent };
//# sourceMappingURL=category-section.component.js.map
