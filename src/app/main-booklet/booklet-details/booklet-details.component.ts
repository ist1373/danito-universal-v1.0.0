import { Component, OnInit } from '@angular/core';
import {BookletService} from '../../service/booklet.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Content} from '../../entity/Content';
import {AppContextUrl, ContentContextUrl, ExpireTime, FileContextUrl} from '../../globals';
import {ProgressHttp} from 'angular-progress-http';
import {LoginRegisterComponent} from '../../login-register/login-register.component';
import {MdDialog, MdDialogConfig, MdSnackBar, MdSnackBarConfig} from '@angular/material';
import {HeaderComponent} from '../../header/header.component';

import {UserService} from '../../service/user.service';
import {plainToClass} from 'class-transformer';
import {User} from '../../entity/User';
import {CacheService} from 'ng2-cache-service';
import {CookieService} from 'ng2-cookies';

import {GalleryService} from 'ng-gallery';

import {Tag} from '../../entity/Tag';
import {Comment} from '../../entity/Comment';
import {CommentsService} from '../../service/comments.service';
import {IStarRatingOnRatingChangeEven} from 'angular-star-rating';
import {FactorDialogComponent} from "../../factor-dialog/factor-dialog.component";
import {Meta} from "@angular/platform-browser";
import {isPlatformBrowser} from "@angular/common";
import {MetaService} from "@ngx-meta/core";


@Component({
  selector: 'app-booklet-details',
  templateUrl: './booklet-details.component.html',
  styleUrls: ['./booklet-details.component.scss'],
  providers:[BookletService,UserService,CacheService,CookieService,CommentsService]
})
export class BookletDetailsComponent implements OnInit {

  booklet = new Content();
  comments: Comment[] = [];
  base = AppContextUrl;
  baseFile = FileContextUrl;
  baseContent = ContentContextUrl;
  token = this.cacheService.get('token');
  user:User = this.cacheService.get('user');
  userComment = new Comment();

  images = [

  ];

  constructor(private userService:UserService,private cacheService: CacheService,private bookletService: BookletService,
              private router: Router, private activatedRoute: ActivatedRoute,public dialog: MdDialog,private gallery: GalleryService,
              private http: ProgressHttp,private commentService:CommentsService,public snackBar: MdSnackBar,
              private readonly metaService: MetaService) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.loadBooklet(params['uid']);


    });
    if (isPlatformBrowser(this.cacheService.get('platform'))) {
      window.scrollTo(0, 0);
    }
    // this.gallery.load(this.images);
    // this.gallery.set(0);
  }

  loadBooklet(uid: string) {
    this.bookletService.getContentByUID(uid).subscribe((booklet) => {
      this.booklet = booklet;
      this.loadComments(booklet.id);
      this.loadFileAccesses();
      this.initSampleImages();
      console.log(booklet);
      this.checkZarinVerification();

      // this.metaService.addTag({ name: 'title', content: this.booklet.title.toString() },true);
      // this.metaService.addTag({ name: 'og:title', content: this.booklet.title.toString() },true);
      // this.metaService.addTag({ name:'og:image',content:this.baseFile + 'fid=' + this.booklet.coverImage.id + '&isThumb=true'});
      // this.metaService.addTag({ name:'og:description',content:this.booklet.category.title + '\n' + this.booklet.university.title.toString() + '\n' + this.booklet.description.toString()});
      // this.metaService.addTag({ name:'og:locale',content:'fa_IR'});
      // this.metaService.addTag({ name:'og:image:width',content:'200px'});
      // this.metaService.addTag({ name:'og:image:height',content:'300px'});

      this.metaService.setTitle(' جزوه ' + this.booklet.title.toString() + ' '+ (this.booklet.university!=null?this.booklet.university.title.toString():'')+ ' '+ (this.booklet.professor!=null?this.booklet.professor.toString():'') ,true);
      this.metaService.setTag( 'og:image',this.baseFile + 'fid=' + this.booklet.coverImage.id + '&isThumb=true');
      this.metaService.setTag('og:description',this.booklet.category.title + '\n' + (this.booklet.university!=null?this.booklet.university.title.toString():'') + '\n' + (this.booklet.description!=null?this.booklet.description.toString():''));
      this.metaService.setTag('og:locale','fa_IR');
      this.metaService.setTag('og:image:width','200px');
      this.metaService.setTag('og:image:height','300px');
    });
  }

  initSampleImages()
  {
    for(let image of this.booklet.images)
    {
     let i={
        src:FileContextUrl+'fid='+image.id,
        thumbnail: FileContextUrl+'fid='+image.id + '&isThumb=true',
        text: image.description
      };
      this.images.push(i);
    }
    this.gallery.load(this.images);
  }

  // downloadFile(fileId:number,bookletId:number)
  // {
  //   let headers = new Headers({});
  //   headers.set("Authorization",UserService.token);
  //   let options = new RequestOptions({ headers: headers});
  //   // var xmlhttp = new XMLHttpRequest();
  //   // xmlhttp.onreadystatechange = function() {
  //   //   if (this.readyState == 4 && this.status == 200) {
  //   //     document.getElementById("demo").innerHTML =
  //   //       this.responseText;
  //   //   }
  //   // };
  //   // xmlhttp.open("GET", "xmlhttp_info.txt", true);
  //   this.http
  //     .withDownloadProgressListener(progress => { console.log(`Downloading ${progress.loaded}%`); })
  //     .get(this.base+'file/book/'+fileId+'?jozveID='+bookletId,options)
  //     .subscribe((response) => {
  //       var blob = new Blob([response["_body"]]);
  //       var url= window.URL.createObjectURL(blob);
  //       window.open(url);
  //       saveAs(blob, "hello world.pdf");
  //       // console.log(response)
  //     })
  // }



  loadFileAccesses()
  {
    // this.cookieService.set("Authorization",UserService.token);
    for(let i=0;i<this.booklet.files.length;i++)
    {
      this.bookletService.getFileStatus(this.booklet.files[i].id,this.booklet.id).subscribe((res)=>{
        console.log(this.booklet.files[i].id + '--------------'+res.status);
        this.booklet.files[i].status = res.status;
      },(error)=>{
        console.log(this.booklet.files[i].id + '--------------'+error.status);
        this.booklet.files[i].status = error.status;
      });
    }
  }

  contentByTag(tag:Tag)
  {
     this.router.navigate(['content','tag',tag.id,tag.tagString]);
  }


  loadComments(id: number) {
    this.bookletService.getCommentsByBooklet(id,0).subscribe((comments) => {
      this.comments = comments;
      console.log(comments);
    });
  }

  addComment(commentStr:string,rate:number){
     this.userComment.description = commentStr;
     let prod = new Content();
     prod.id = this.booklet.id;
     this.userComment.product = prod;
     this.commentService.addComment(this.userComment).subscribe((res)=>{
       console.log(res);
       if(res.json()['success']==true)
         this.openSnackBar(' پیام شما با موفقیت ثبت شد.بعد از بررسی منتشر می شود','success');
       else
         this.openSnackBar(res.json()['message'],'error');
     });
  }

  openSnackBar(message:string,cssClass:string) {
    let conf = new MdSnackBarConfig();
    conf.duration = 5000;
    conf.extraClasses = [cssClass];
    this.snackBar.open(message,'',conf);
  }


  onRatingChange = ($event:IStarRatingOnRatingChangeEven) => {
    console.log('onRatingUpdated $event: ', $event);
    this.userComment.rate = $event.rating;
  };



  // initIconColors() {
  //
  //   var imgs = ["#img1", "#img2", "#img3", "#img4", "#img5", "#img6", "#img7", "#img8"]
  //   for (let img of imgs) {
  //     Caman(img, function () {
  //       // We can call any filter before the layer
  //       this.newLayer(function () {
  //         // Change the blending mode
  //         this.setBlendingMode("normal");
  //         // Change the opacity of this layer
  //         this.opacity(100);
  //         // Now we can *either* fill this layer with a
  //         // solid color...
  //         // this.fillColor('#53346D');
  //         this.fillColor('#aabb34');
  //         this.copyParent();
  //       });
  //       this.render();
  //     });
  //   }
  //
  // }

  openLogin(){
    let conf = new MdDialogConfig()
    conf.height = '400px';
    let dialogRef = this.dialog.open(LoginRegisterComponent,conf);
    dialogRef.afterClosed().subscribe(()=>{
      if(dialogRef.componentInstance.status==200)
      {
        console.log('fuccccc');
        this.userService.getUser().subscribe((res)=>{
          let user = plainToClass<User,Object>(User,res.json());
          this.userService.userChange.next(user);
          this.cacheService.set('user',user,{expires: Date.now() + ExpireTime});
          if (isPlatformBrowser(this.cacheService.get('platform'))) {
            window.location.reload();
          }
        });
      }

    });
  }


  openFactorDialog(){
    // this.bookletService.connectToZarinpalGateway(this.booklet.price,window.location.href,this.booklet.title.toString()).subscribe((res)=>{
    //   window.location.href = 'https://www.zarinpal.com/pg/StartPay/' + res.json()['message'];
    // });
    let conf = new MdDialogConfig();
    // conf.height = '500px';
    let dialogRef = this.dialog.open(FactorDialogComponent,conf);
    dialogRef.componentInstance.content = this.booklet;
  }
  checkZarinVerification()
  {

    this.activatedRoute.queryParams.subscribe((params: Params) => {

      if(params['Authority']!=null)
      {
        let user=this.cacheService.get('user');
        this.bookletService.verifyZarinPayment(this.booklet.price,user.uid,this.booklet.uid,params['Authority']).subscribe((res)=>{
          //
          this.openSnackBar("خرید جزوه با موفقیت انجام شد","success");
          this.loadFileAccesses();
        });

      }

    });
  }


}







