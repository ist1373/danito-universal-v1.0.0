import {Component, Input, OnInit} from '@angular/core';
import {Content} from "../../entity/Content";
import {SpecialContent} from "../../entity/model/SpecialContent";
import {AppContextUrl, FileContextUrl} from "../../globals";

@Component({
  selector: 'app-special-booklet',
  templateUrl: './special-booklet.component.html',
  styleUrls: ['./special-booklet.component.scss']
})
export class SpecialBookletComponent implements OnInit {
  base = AppContextUrl;
  baseFile = FileContextUrl;
  @Input() specialBooklet:SpecialContent;
  beautifyTitle = '';
  constructor() { }

  ngOnInit() {
    if(this.specialBooklet!=null)
      this.beautifyTitle = this.specialBooklet.content.title.replace(/ /g,'-')
  }

}
