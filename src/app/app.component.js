var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { BooksumCacheService } from "./booksum-cache.service";
import { CacheService } from "ng2-cache-service";
import { UserService } from "./user.service";
import { UniversityService } from "./university.service";
import { HttpInterceptorService } from "ng-http-interceptor";
import { SlimLoadingBarService } from "ng2-slim-loading-bar";
// import {CacheStoragesEnum} from "ng2-cache-service/dist/src/enums/cache-storages.enum";
// declare var init:any;
var AppComponent = (function () {
    function AppComponent(booksumCacheService, cacheService, httpInterceptor, slimLoadingBarService) {
        this.booksumCacheService = booksumCacheService;
        this.cacheService = cacheService;
        this.slimLoadingBarService = slimLoadingBarService;
        this.title = 'app works!';
        // this.cacheService.useStorage(CacheStoragesEnum.LOCAL_STORAGE);
        // init();
        httpInterceptor.request().addInterceptor(function (data, method) {
            // console.log(method, data);
            slimLoadingBarService.start();
            return data;
        });
        httpInterceptor.response().addInterceptor(function (res, method) {
            return res.do(function (r) {
                slimLoadingBarService.complete();
            });
        });
    }
    AppComponent.prototype.ngOnInit = function () {
        // this.booksumCacheService.loadCaches();
    };
    return AppComponent;
}());
AppComponent = __decorate([
    Component({
        selector: 'app-root',
        templateUrl: './app.component.html',
        styleUrls: ['./app.component.css'],
        providers: [BooksumCacheService, CacheService, UserService, UniversityService]
    }),
    __metadata("design:paramtypes", [BooksumCacheService, CacheService, HttpInterceptorService, SlimLoadingBarService])
], AppComponent);
export { AppComponent };
//# sourceMappingURL=app.component.js.map