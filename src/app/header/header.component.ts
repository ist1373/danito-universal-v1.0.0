import {Component, OnInit, ViewChild, ElementRef, Output, ViewContainerRef, Input} from '@angular/core';
import {Router} from '@angular/router';
// import { Overlay } from 'angular2-modal';
// import { Modal } from 'angular2-modal/plugins/bootstrap';
import {CategoryService} from '../category-dialog/category-service';
import {Category} from '../entity/Category';
import {plainToClass} from 'class-transformer/index';
import {Response} from '@angular/http';
import {MdDialog, MdDialogConfig, MdDialogRef} from '@angular/material';
import {CategoryDialogComponent} from '../category-dialog/category-dialog.component';
import {LoginRegisterComponent} from '../login-register/login-register.component';
import {UserService} from '../service/user.service';
import {Content} from '../entity/Content';
import {User} from '../entity/User';
import {AppContextUrl, ExpireTime, FileContextUrl} from '../globals';
import {BooksumCacheService} from '../service/booksum-cache.service';
import {CacheService} from 'ng2-cache-service';
import {isPlatformBrowser} from "@angular/common";



declare var jQuery: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [CacheService, CategoryService, UserService, BooksumCacheService]
  }
)
export class HeaderComponent implements OnInit {
  private city: any|null;
  categories: Category[] = [];
  categoryHeader= '';
  user: User;

  token = this.cacheService.get('token');
  base= AppContextUrl;
  fileBase = FileContextUrl;
  _subscription: any;

  constructor(private booksumCacheService: BooksumCacheService, private userService: UserService,
              private cacheService: CacheService, private router: Router, private categoryService: CategoryService,
              public dialog: MdDialog) {
    this.city  = this.cacheService.get('city');

  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  ngOnInit() {
    this._subscription = this.userService.userChange.subscribe((user) => {

      this.user = user;
    });
    if (this.cacheService.exists('user'))
    {
      this.user = this.cacheService.get('user');
      console.log(this.user);
    }
  }


  openModal(){
    this.categoryService.getParentsCategory().subscribe((data: Response) => {
      const dialogRef = this.dialog.open(CategoryDialogComponent);
      dialogRef.componentInstance.categories = plainToClass(Category, data.json());
      dialogRef.componentInstance.categoryHeader = 'دسته بندی';
    });
  }

  logOut(){
    this.cacheService.remove('user');
    this.cacheService.remove('token');
    this.userService.userChange.next(null);
    if (isPlatformBrowser(this.cacheService.get('platform'))) {
      window.location.reload();
    }
  }


  openLoginRegisterModal(isSignIn: boolean)
  {
    let conf = new MdDialogConfig();
    conf.height = '400px';
    const dialogRef = this.dialog.open(LoginRegisterComponent,conf);
    dialogRef.componentInstance.signInTap = isSignIn;
    // dialogRef.afterClosed().subscribe(()=>{
    //     if(dialogRef.componentInstance.status==200)
    //     {
    //       this.userService.getFinalUser().subscribe((res)=>{
    //         this.user = plainToClass<User,Object>(User,res.json());
    //
    //         this.cacheService.set("user",this.user,{expires: Date.now() + ExpireTime});
    //       });
    //     }
    //
    // });
  }


  checkUserAuthority(authority:string)
  {
    if(this.user != null)
    {
      for (let auth of this.user.authorities)
      {
        if (auth.name == authority)
          return true;
      }
      return false;
    }
    else{
      return false;
    }
  }




}



