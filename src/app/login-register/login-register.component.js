var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { UserService } from "../user.service";
import { MdDialogRef, MdSnackBar, MdSnackBarConfig } from "@angular/material";
import { CacheService } from "ng2-cache-service";
import { ExpireTime } from "../globals";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { CustomValidators } from "ng2-validation";
import { plainToClass } from "class-transformer";
import { User } from "../entity/User";
var LoginRegisterComponent = (function () {
    function LoginRegisterComponent(snackBar, cacheService, userService, dialogRef) {
        this.snackBar = snackBar;
        this.cacheService = cacheService;
        this.userService = userService;
        this.dialogRef = dialogRef;
        this.logmessage = "";
        this.regmessage = "";
        this.status = 401;
        this.pass = new FormControl('', Validators.required);
        this.passrep = new FormControl('', CustomValidators.equalTo(this.pass));
        this.uname = new FormControl('', Validators.minLength(6));
        this.email = new FormControl('', CustomValidators.email);
        this.complexForm = new FormGroup({
            uname: this.uname,
            pass: this.pass,
            passrep: this.passrep,
            email: this.email
        });
    }
    LoginRegisterComponent.prototype.ngOnInit = function () {
    };
    LoginRegisterComponent.prototype.login = function (email, password) {
        var _this = this;
        console.log(email + "  " + password);
        this.userService.login(email, password).subscribe(function (res) {
            // UserService.token =res.json()['token'];
            _this.cacheService.set("token", res.json()['token'], { expires: Date.now() + ExpireTime });
            _this.status = 200;
            _this.getFinalUser();
        }, function (error) {
            _this.logmessage = "پست الکترونیک یا رمز عبور اشتباه است.";
        });
    };
    LoginRegisterComponent.prototype.getFinalUser = function () {
        var _this = this;
        this.userService.getFinalUser().subscribe(function (res) {
            var user = plainToClass(User, res.json());
            _this.cacheService.remove("user");
            _this.cacheService.set("user", user, { expires: Date.now() + ExpireTime });
            _this.dialogRef.close();
            window.location.reload();
        });
    };
    LoginRegisterComponent.prototype.register = function (email, username, pass, value) {
        var _this = this;
        console.log(value);
        this.userService.registerFinalUser(email, username, pass).subscribe(function (res) {
            console.log(res);
            if (res.json()['success'] != true) {
                _this.regmessage = res.json()['message'];
            }
            else {
                _this.dialogRef.close();
                _this.openSnackBar();
            }
        }, function (error) {
            _this.regmessage = error.toString();
        });
    };
    LoginRegisterComponent.prototype.openSnackBar = function () {
        var conf = new MdSnackBarConfig();
        conf.duration = 5000;
        conf.extraClasses = ["success"];
        this.snackBar.open("ثبت نام شما با موفقیت انجام شد", "", conf);
    };
    return LoginRegisterComponent;
}());
LoginRegisterComponent = __decorate([
    Component({
        selector: 'app-login-register',
        templateUrl: './login-register.component.html',
        styleUrls: ['./login-register.component.scss'],
        providers: [UserService, CacheService]
    }),
    __metadata("design:paramtypes", [MdSnackBar, CacheService, UserService, MdDialogRef])
], LoginRegisterComponent);
export { LoginRegisterComponent };
//# sourceMappingURL=login-register.component.js.map
