import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {UserService} from '../service/user.service';
import {MdDialogRef, MdSnackBar, MdSnackBarConfig} from '@angular/material';
import {CacheService} from 'ng2-cache-service';
import {ExpireTime} from '../globals';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {plainToClass} from 'class-transformer';
import {User} from '../entity/User';
import {Student} from "../entity/Student";
import {Ng2DeviceService} from "ng2-device-detector";
import {isPlatformBrowser} from "@angular/common";


@Component({
  selector: 'app-login-register',
  templateUrl: './login-register.component.html',
  styleUrls: ['./login-register.component.scss'],
  providers: [UserService, CacheService]
})
export class LoginRegisterComponent implements OnInit {
  logmessage = '';
  regmessage = '';
  status = 401;
  complexForm : FormGroup;

  signInTap = true;

   pass = new FormControl('', Validators.required);
   passrep = new FormControl('', CustomValidators.equalTo(this.pass));
   uname = new FormControl('', Validators.minLength(6));
   email = new FormControl('', CustomValidators.email);

  constructor(private deviceService: Ng2DeviceService,public snackBar: MdSnackBar, private cacheService: CacheService, private userService: UserService, public dialogRef: MdDialogRef<LoginRegisterComponent>) {

    this.complexForm = new FormGroup({
      uname: this.uname,
      pass: this.pass,
      passrep: this.passrep,
      email: this.email
    });
  }


  ngOnInit() {

    // // deviceId:string;
    // //
    // // systemOS:string;
    // //
    // // systemVersion:string;
    // //
    // // systemDevice:string;
    //
    //
    // console.log(this.deviceService.browser);
    // console.log(this.deviceService.browser_version);
    //
    // console.log(this.deviceService.os_version);
  }



  login(email: string, password: string)
  {
    console.log(email + '  ' + password);
    this.userService.login(email, password).subscribe((res) => {

      // UserService.token =res.json()['token'];
      this.cacheService.set('token', res.json()['token'], {expires: Date.now() + ExpireTime});
      this.status = 200;
      this.getUser();
    }, (error) => {
      this.logmessage = 'پست الکترونیک یا رمز عبور اشتباه است.';
    });
  }

  getUser()
  {
    this.userService.getUser().subscribe((res) => {
      let user = plainToClass<Student, Object>(Student, res.json());
      this.cacheService.remove('user');
      this.cacheService.set('user', user, {expires: Date.now() + ExpireTime});
      console.log(user);
      this.dialogRef.close();
      if (isPlatformBrowser(this.cacheService.get('platform'))) {
        window.location.reload();
      }
    });
  }




  register(email: string, username: string, pass: string, value: any)
  {

    let user  = new Student();
    user.username = username;
    user.password = pass;
    user.email = email;
    user.systemOS = this.deviceService.os_version;
    user.systemVersion = this.deviceService.browser + "-" + this.deviceService.browser_version
    user.systemDevice = this.deviceService.device;
    // deviceId:string;
    //
    // systemOS:string;
    //
    // systemVersion:string;
    //
    // systemDevice:string;

    console.log(user);


    this.userService.registerFinalUser(user).subscribe((res) => {
      console.log(res);
      if (res.json()['success'] != true)
      {

        this.regmessage = res.json()['message'];
      }
      else
      {
        this.dialogRef.close();
        this.openSnackBar();
      }

    }, (error) => {
       this.regmessage = error.toString();
    });
  }


  openSnackBar() {
    let conf = new MdSnackBarConfig();
    conf.duration = 5000;
    conf.extraClasses = ['success'];
    this.snackBar.open('ثبت نام شما با موفقیت انجام شد', '', conf);
  }
}
