var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { CommonModule } from "@angular/common";
import { SliderComponent } from './slider/slider.component';
import { FooterComponent } from './footer/footer.component';
import { BookComponent } from './main-book/book/book.component';
import { BookDetailsComponent } from './main-book/book-details/book-details.component';
import { CountryMapComponent } from './country-map/country-map.component';
import { BookletComponent } from './main-booklet/booklet/booklet.component';
import { BookletCarouselComponent } from './main-booklet/booklet-carousel/booklet-carousel.component';
import { MainBookletComponent } from './main-booklet/main-booklet.component';
import { BookCarouselComponent } from './main-book/book-carousel/book-carousel.component';
import { MainBookComponent } from './main-book/main-book.component';
import { SpecialBookletComponent } from './main-booklet/special-booklet/special-booklet.component';
import { CategorySectionComponent } from './main-booklet/category-section/category-section.component';
import { CategoryItemComponent } from './main-booklet/category-section/category-item/category-item.component';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import 'hammerjs';
import { MdChipsModule, MdDialogModule, MdProgressBarModule, MdSnackBarModule, MdTabsModule, MdTooltipModule } from "@angular/material";
import { BookletListComponent } from './main-booklet/booklet-list/booklet-list.component';
import { BookletDetailsComponent } from './main-booklet/booklet-details/booklet-details.component';
import { StarRatingModule } from 'angular-star-rating';
import { TermComponent } from './term/term.component';
import { CategoryDialogComponent } from "./category-dialog/category-dialog.component";
import { LoginRegisterComponent } from './login-register/login-register.component';
import { LocalStorageServiceModule } from "ng2-cache-service";
import { ProgressHttpModule } from "angular-progress-http";
import { GalleryModule } from "ng-gallery";
import { NewBookletComponent } from './main-booklet/new-booklet/new-booklet.component';
import { SelectModule } from "ng2-select";
import { FileUploadModule } from "ng2-file-upload";
import { RlTagInputModule } from "angular2-tag-input/dist";
import { SlimLoadingBarModule } from "ng2-slim-loading-bar";
import { HttpInterceptorModule } from "ng-http-interceptor";
var appRoutes = [
    { path: '', redirectTo: 'booklet', pathMatch: 'full' },
    { path: 'booklet', component: MainBookletComponent },
    { path: 'booklet/list/:group', component: BookletListComponent },
    { path: 'booklet/cat/:id', component: BookletListComponent },
    { path: 'booklet/cat/:id/:name', component: BookletListComponent },
    { path: 'booklet/new', component: NewBookletComponent },
    { path: 'booklet/:id/:name', component: BookletDetailsComponent },
    { path: 'booklet/:id', component: BookletDetailsComponent },
    { path: 'selectcity', component: CountryMapComponent },
    { path: 'book/:city', component: MainBookComponent },
    { path: 'book/:city/details', component: BookDetailsComponent },
    { path: 'terms', component: TermComponent }
    // { path: 'home/:city', component: MainPageComponent,children:ChildRoutes}
];
export var galleryConfig = {
    "style": {
        "background": "#121519",
        "width": "100%",
        "height": "100%"
    },
    "animation": "fade",
    "loader": {
        "width": "50px",
        "height": "50px",
        "position": "center",
        "icon": "oval"
    },
    "description": {
        "position": "bottom",
        "overlay": false,
        "text": true,
        "counter": true
    },
    "bullets": false,
    "player": {
        "autoplay": false,
        "speed": 3000
    },
    "thumbnails": {
        "width": 50,
        "height": 70,
        "position": "top",
        "space": 10
    },
    "navigation": true
};
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    NgModule({
        declarations: [
            AppComponent,
            HeaderComponent,
            SliderComponent,
            FooterComponent,
            BookComponent,
            BookDetailsComponent,
            CountryMapComponent,
            BookletComponent,
            BookletCarouselComponent,
            MainBookletComponent,
            BookCarouselComponent,
            MainBookComponent,
            SpecialBookletComponent,
            CategorySectionComponent,
            CategoryItemComponent,
            BookletListComponent,
            BookletDetailsComponent,
            TermComponent,
            CategoryDialogComponent,
            LoginRegisterComponent,
            NewBookletComponent
        ],
        entryComponents: [CategoryDialogComponent, LoginRegisterComponent],
        imports: [
            RlTagInputModule,
            HttpInterceptorModule,
            MdChipsModule,
            FormsModule,
            ReactiveFormsModule,
            MdSnackBarModule,
            HttpModule,
            CommonModule,
            BrowserAnimationsModule,
            MdTooltipModule,
            MdTabsModule,
            MdDialogModule,
            StarRatingModule,
            ProgressHttpModule,
            SelectModule,
            FileUploadModule,
            MdProgressBarModule,
            LocalStorageServiceModule,
            SlimLoadingBarModule.forRoot(),
            GalleryModule.forRoot(galleryConfig),
            RouterModule.forRoot(appRoutes),
        ],
        providers: [],
        bootstrap: [AppComponent]
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=app.module.js.map