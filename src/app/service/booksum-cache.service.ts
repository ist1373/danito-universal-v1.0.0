import { Injectable } from '@angular/core';
// import {CacheService, CacheStoragesEnum} from 'ng2-cache/ng2-cache';
import {UserService} from "./user.service";
import {CacheService} from "ng2-cache-service";
import {UniversityService} from "./university.service";
import {University} from "../entity/University";

@Injectable()
export class BooksumCacheService {

  universities:Array<string>;

  constructor(private cacheService: CacheService,private userService:UserService,private universityService:UniversityService) {


  }

  loadCaches()
  {
    // console.log(this.cacheService.get("token"));
    // if(this.cacheService.exists("token"))
    // {
    //   UserService.token = this.cacheService.get("token");
    // }
    // if(this.cacheService.exists("user"))
    // {
    //   UserService.user = this.cacheService.get("user");
    //   this.userService.userChange.next(UserService.user);
    //   console.log(UserService.user);
    // }
  }
  initUniversirsities()
  {
    if(this.cacheService.exists("universities"))
    {
      this.universities = this.cacheService.get("universities");
      console.log(this.universities);
    }
    else
    {
      this.universityService.getUniversities().subscribe((unis:University[])=>{

        for(let uni of unis)
        {
          this.universities.push(uni.title.toString());
        }

      })
      this.cacheService.set("universities",this.universities);
    }

  }

}
