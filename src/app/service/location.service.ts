import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {AppContextUrl} from "../globals";
import {plainToClass} from "class-transformer";
import {Province} from "../entity/Province";
import {City} from "../entity/City";

@Injectable()
export class LocationService {

  constructor(private http: Http) { }

  getProvinces(){
    return this.http.get(AppContextUrl + 'location/province').map((res)=>{
      let provinces = plainToClass<Province,Object[]>(Province,res.json());
      return provinces;
    })
  }

  getCities(provinceId:number){
    return this.http.get(AppContextUrl + 'location/province/' + provinceId + '/city').map((res) =>{
      let cities = plainToClass<City,Object[]>(City,res.json());
      return cities;
    })
  }
}
