import { Injectable } from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import {CacheService} from "ng2-cache-service";
import {AppContextUrl, ItemSizePerPage} from "../globals";
import {plainToClass} from "class-transformer";
import {Book, BookState} from "../entity/Book";
import {ProductMarket} from "../entity/model/ProductMarket";
import {CallInfo} from "../entity/CallInfo";

@Injectable()
export class BookService {

  constructor(private http: Http,private cacheService:CacheService) { }


  getBookByID(id:number) {
    return this.http.get(AppContextUrl + 'book/' + id).map(res => {
      let book = plainToClass<Book,Object>(Book,res.json());
      return book;
    });
  }

  editBook(book:Book)
  {
    return this.http.put(AppContextUrl+'book',book,this.setAuthorizationHeader());
  }

  saveBook(book:Book)
  {
    return this.http.post(AppContextUrl+'book',book,this.setAuthorizationHeader());
  }

  getBookMarket() {
    return this.http.get(AppContextUrl + 'market/book').map((res)=>{
      var bookMarket = plainToClass<ProductMarket, Object>(ProductMarket, res.json());
      return bookMarket;
    });
  }



  findBook(title:string,author:string,translator:string,publisher:string,publishYear:string,bookState:BookState,universityId:number,categoryId:number,cityId:number, page:number)
  {
    return this.http.get(AppContextUrl+'book?title='+title+'&author='+author+'&translator='+translator+'&publisher='+publisher+
      '&publishYear='+publishYear+ '&bookState='+bookState +'&universityId='+universityId+
      '&categoryId='+categoryId+'&cityId='+cityId+'&page='+page+'&size='+ItemSizePerPage,this.setAuthorizationHeader()).map((res) =>{
      let books= plainToClass<Book,Object[]>(Book,res.json());
      return books;
    });
  }

  getCallInfoByBook(id:number){
    return this.http.get(AppContextUrl + 'callInfo?bookId=' + id).map((res)=>{
      var callInfo = plainToClass<CallInfo, Object>(CallInfo, res.json());
      return callInfo;
    });
  }


  setAuthorizationHeader()
  {
    let headers = new Headers({});
    headers.set('Authorization',this.cacheService.get('token'));
    let options = new RequestOptions({ headers: headers});
    return options;
  }
}
