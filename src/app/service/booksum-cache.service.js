var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
// import {CacheService, CacheStoragesEnum} from 'ng2-cache/ng2-cache';
import { UserService } from "../user.service";
import { CacheService } from "ng2-cache-service";
import { UniversityService } from "../university.service";
var BooksumCacheService = (function () {
    function BooksumCacheService(cacheService, userService, universityService) {
        this.cacheService = cacheService;
        this.userService = userService;
        this.universityService = universityService;
    }
    BooksumCacheService.prototype.loadCaches = function () {
        // console.log(this.cacheService.get("token"));
        // if(this.cacheService.exists("token"))
        // {
        //   UserService.token = this.cacheService.get("token");
        // }
        // if(this.cacheService.exists("user"))
        // {
        //   UserService.user = this.cacheService.get("user");
        //   this.userService.userChange.next(UserService.user);
        //   console.log(UserService.user);
        // }
    };
    BooksumCacheService.prototype.initUniversirsities = function () {
        var _this = this;
        if (this.cacheService.exists("universities")) {
            this.universities = this.cacheService.get("universities");
            console.log(this.universities);
        }
        else {
            this.universityService.getUniversities().subscribe(function (unis) {
                for (var _i = 0, unis_1 = unis; _i < unis_1.length; _i++) {
                    var uni = unis_1[_i];
                    _this.universities.push(uni.title.toString());
                }
            });
            this.cacheService.set("universities", this.universities);
        }
    };
    return BooksumCacheService;
}());
BooksumCacheService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [CacheService, UserService, UniversityService])
], BooksumCacheService);
export { BooksumCacheService };
//# sourceMappingURL=booksum-cache.service.js.map
