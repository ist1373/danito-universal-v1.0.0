// add base url from string
import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, ResponseContentType,URLSearchParams } from '@angular/http';
import {AppContextUrl, AppContextUrlV2, ItemSizePerPage, MerchantID, PaymentZarinpal} from '../globals';
import {plainToClass} from 'class-transformer';
import {Content, ProductState} from '../entity/Content';
import {ProgressHttp} from 'angular-progress-http';
import {ZarinpalPayment} from '../entity/Payment/ZarinpalPayment';
import {UserService} from './user.service';
import {CacheService} from 'ng2-cache-service';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';
import {Comment} from '../entity/Comment';





@Injectable()
export class BookletService  {
  constructor(private http:Http,private cacheService:CacheService) {

  }

  public static tempVar= new Map<String,any>();

  getContentMarket() {
    return this.http.get(AppContextUrl + 'market/content');
  }

  getContentByID(id:number) {

    return this.http.get(AppContextUrl + 'content/' + id,this.setAuthorizationHeader()).map((res)=>{
      let content = plainToClass<Content,Object>(Content,res.json());
      return content;
    });
  }
  getContentByUID(uid:string) {

    return this.http.get(AppContextUrlV2 + 'content/' + uid,this.setAuthorizationHeader()).map((res)=>{
      let content = plainToClass<Content,Object>(Content,res.json());
      return content;
    });
  }
  getContentsByURL(url:string) {
    return this.http.get(url).map((res) =>{
      let contents= plainToClass<Content,Object[]>(Content,res.json());
      return contents;
    });
  }

  getContentsByCategory(id:number, page:number) {
    return this.http.get(AppContextUrl+'content?catId='+id+'&page='+page+'&size='+ItemSizePerPage).map((res) =>{
      let contents= plainToClass<Content,Object[]>(Content,res.json());
      return contents;
    });
  }

  getContentsByState(state:ProductState, page:number)
  {
    return this.http.get(AppContextUrl+'content?pState='+ProductState[state]+'&page='+page+
      '&size='+ItemSizePerPage,this.setAuthorizationHeader()).map((res) =>{
      let contents= plainToClass<Content,Object[]>(Content,res.json());
      return contents;
    });
  }


  findContent(title:string,professor:string,writeYear:number,universityId:number,categoryId:number, page:number)
  {
    return this.http.get(AppContextUrl+'content?title='+title+'&professor='+professor+'&writeYear='+writeYear+'&universityId='+universityId+
      '&categoryId='+categoryId+'&page='+page+'&size='+ItemSizePerPage,this.setAuthorizationHeader()).map((res) =>{
      let contents= plainToClass<Content,Object[]>(Content,res.json());
      return contents;
    });
  }


  getContentsByStateAndCategory(state:ProductState, catId:number, page:number)
  {
    return this.http.get(AppContextUrl+'content?pState='+ProductState[state]+'&catId='+catId+
      '&page='+page+'&size='+ItemSizePerPage,this.setAuthorizationHeader()).map((res) =>{
      let contents= plainToClass<Content,Object[]>(Content,res.json());
      return contents;
    });
  }



  getContentsByTag(id:number, page:number)
  {
    return this.http.get(AppContextUrl+'content/tag/'+id+'?'+'page='+page+'&size='+ItemSizePerPage).map((res) =>{
      let contents= plainToClass<Content,Object[]>(Content,res.json());
      return contents;
    });
  }

  getDownloadedContentsByUser(page:number){
    return this.http.get(AppContextUrl + 'content/downloaded'+'?'+'page='+page+'l&size='+ItemSizePerPage,this.setAuthorizationHeader()).map((res)=>{
      let contents= plainToClass<Content,Object[]>(Content,res.json());
      return contents;
    });
  }
  getMyContents(page:number){
    return this.http.get(AppContextUrl + 'content/myContent'+'?'+'page='+page+'l&size='+ItemSizePerPage,this.setAuthorizationHeader()).map((res)=>{
      let contents= plainToClass<Content,Object[]>(Content,res.json());
      return contents;
    });
  }




  getCommentsByBooklet(id:number,page:number) {
    return this.http.get(AppContextUrl + 'comment?contentId=' + id+'&page='+page+'&size='+ItemSizePerPage).map((res) =>{
      let Comments= plainToClass<Comment,Object[]>(Comment,res.json());
      return Comments;
    });
  }

  // downloadFile(fileId:number,jozveId:number)
  // {
  //   return this.http.get(AppContextUrl+"file/book/"+fileId+"?jozveID="+jozveId)
  //     .subscribe((response) => {
  //      console.log(response);
  //     },(error)=>{
  //       console.log(error + "kkkkkkk");
  //     });
  // }

  saveJozve(jozve:Content)
  {
    return this.http.post(AppContextUrl+'content',jozve,this.setAuthorizationHeader());
  }

  editJozve(jozve:Content)
  {
    return this.http.put(AppContextUrl+'content',jozve,this.setAuthorizationHeader());
  }

  editJozveState(id:number,state:ProductState)
  {
    return this.http.put(AppContextUrl+'content/'+id+'?pState='+ProductState[state],null,this.setAuthorizationHeader());
  }

  getFileStatus(fileId:number,jozveId:number)
  {
    if(this.cacheService.get('token') !=null)
    {
      return this.http.get(AppContextUrl+'file/contentFileStatus/'+fileId+'?contentId='+jozveId,this.setAuthorizationHeader());
    }
    else
    {
      return this.http.get(AppContextUrl+'file/contentFileStatus/'+fileId+'?contentId='+jozveId);
    }
  }

  connectToZarinpalGateway(amount:number,callbackURL:string,description:string)
  {
    let params: URLSearchParams = new URLSearchParams();
    params.set('amount', amount.toString());
    params.set('description', description);
    params.set('callbackUrl', callbackURL);
    params.set('mobile', '');
    let user = this.cacheService.get('user');
    params.set('email', user.email);
    return this.http.post(AppContextUrl + 'transaction/paymentRequestZarin',params);
  }

  verifyZarinPayment(amount:number,userUId:string,productUId:string,authority:string)
  {
    let params: URLSearchParams = new URLSearchParams();
    params.set('amount', amount.toString());
    params.set('userUid', userUId);
    params.set('contentUid', productUId);
    params.set('authority', authority);
    return this.http.post(AppContextUrl + 'transaction/verifyContentZarin',params);
  }

  setAuthorizationHeader()
  {
    let headers = new Headers({});
    headers.set('Authorization',this.cacheService.get('token'));
    let options = new RequestOptions({ headers: headers});
    return options;
  }


}

