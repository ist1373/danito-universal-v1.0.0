var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { CacheService } from "ng2-cache-service";
import { Http } from "@angular/http";
import { AppContextUrl } from "../globals";
import { plainToClass } from "class-transformer";
import { University } from "../entity/University";
var UniversityService = (function () {
    function UniversityService(http, cacheService) {
        this.http = http;
        this.cacheService = cacheService;
    }
    UniversityService.prototype.getUniversities = function () {
        return this.http.get(AppContextUrl + "university").map(function (res) {
            var unis = plainToClass(University, res.json());
            return unis;
        });
    };
    return UniversityService;
}());
UniversityService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Http, CacheService])
], UniversityService);
export { UniversityService };
//# sourceMappingURL=university.service.js.map
