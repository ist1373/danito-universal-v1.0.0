var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// add base url from string
import { Injectable } from "@angular/core";
import { Headers, Http, RequestOptions, URLSearchParams } from "@angular/http";
import { AppContextUrl, ItemSizePerPage } from "../globals";
import { plainToClass } from "class-transformer";
import { Content } from "../entity/Content";
import { CacheService } from "ng2-cache-service";
import { SlimLoadingBarService } from "ng2-slim-loading-bar";
var MainBookletService = (function () {
    function MainBookletService(http, cacheService, slimLoadingBarService) {
        this.http = http;
        this.cacheService = cacheService;
        this.slimLoadingBarService = slimLoadingBarService;
    }
    MainBookletService.prototype.getBookletMarket = function () {
        // this.slimLoadingBarService.start();
        return this.http.get(AppContextUrl + 'market/book');
    };
    MainBookletService.prototype.getBookByID = function (id) {
        return this.http.get(AppContextUrl + 'book/' + id);
    };
    MainBookletService.prototype.getBookletByID = function (id) {
        return this.http.get(AppContextUrl + 'book/' + id).map(function (res) {
            var booklet = plainToClass(Content, res.json());
            return booklet;
        });
    };
    MainBookletService.prototype.getBookletsByURL = function (url) {
        return this.http.get(url).map(function (res) {
            var booklets = plainToClass(Content, res.json());
            return booklets;
        });
    };
    MainBookletService.prototype.getBookletsByCategory = function (id, page) {
        return this.http.get(AppContextUrl + "book?catId=" + id + '&page=' + page + '&size=' + ItemSizePerPage).map(function (res) {
            var booklets = plainToClass(Content, res.json());
            return booklets;
        });
    };
    MainBookletService.prototype.getCommentsByBooklet = function (id) {
        return this.http.get(AppContextUrl + 'comment?jozveId=' + id).map(function (res) {
            var Comments = plainToClass(Comment, res.json());
            return Comments;
        });
    };
    MainBookletService.prototype.downloadFile = function (fileId, jozveId) {
        return this.http.get(AppContextUrl + "file/book/" + fileId + "?jozveID=" + jozveId)
            .subscribe(function (response) {
            console.log(response);
        }, function (error) {
            console.log(error + "kkkkkkk");
        });
    };
    MainBookletService.prototype.saveJozve = function (jozve) {
        var headers = new Headers({});
        headers.set("Authorization", this.cacheService.get("token"));
        // headers.set("Content-Type","application/x-www-form-urlencoded");
        var options = new RequestOptions({ headers: headers });
        return this.http.post(AppContextUrl + "content", jozve, options);
    };
    MainBookletService.prototype.getFileStatus = function (fileId, jozveId) {
        if (this.cacheService.get("token") != null) {
            console.log("loginnnnnnnnn");
            var headers = new Headers({});
            headers.set("Authorization", this.cacheService.get("token"));
            var options = new RequestOptions({ headers: headers });
            console.log(AppContextUrl + "file/jozveStatus/" + fileId + "?jozveID=" + jozveId);
            return this.http.get(AppContextUrl + "file/jozveStatus/" + fileId + "?jozveID=" + jozveId, options);
        }
        else {
            return this.http.get(AppContextUrl + "file/jozveStatus/" + fileId + "?jozveID=" + jozveId);
        }
    };
    MainBookletService.prototype.connectToZarinpalGateway = function (amount, callbackURL, description) {
        var params = new URLSearchParams();
        params.set('amount', amount.toString());
        params.set('description', description);
        params.set('callbackUrl', callbackURL);
        params.set('mobile', "");
        var user = this.cacheService.get("user");
        params.set('email', user.email);
        return this.http.post(AppContextUrl + "transaction/paymentRequestZarin", params);
    };
    MainBookletService.prototype.verifyZarinPayment = function (amount, userId, productId, authority) {
        var params = new URLSearchParams();
        params.set('amount', amount.toString());
        params.set('userId', userId.toString());
        params.set('productId', productId.toString());
        params.set('authority', authority);
        return this.http.post(AppContextUrl + "transaction/verifyJozveZarin", params);
    };
    return MainBookletService;
}());
MainBookletService.tempVar = new Map();
MainBookletService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Http, CacheService, SlimLoadingBarService])
], MainBookletService);
export { MainBookletService };
//# sourceMappingURL=main-booklet.service.js.map
