import {Routes} from '@angular/router';
import {MainBookletComponent} from './main-booklet/main-booklet.component';
import {BookletListComponent} from './main-booklet/booklet-list/booklet-list.component';
import {ModifyBookletComponent} from './admin-pannel/modify-booklet/modify-booklet.component';
import {BookletDetailsComponent} from './main-booklet/booklet-details/booklet-details.component';
import {TermComponent} from './term/term.component';
import {AuthGuard} from './guards/auth.guard';
import {AccessDeniedComponent} from './access-denied/access-denied.component';
import {AdminPannelComponent} from './admin-pannel/admin-pannel.component';
import {CommentsListComponent} from './admin-pannel/comments-list/comments-list.component';
import {AdminBookletListComponent} from './admin-pannel/admin-booklet-list/admin-booklet-list.component';
import {ContactUsComponent} from './contact-us/contact-us.component';
import {UserDetailsComponent} from './user-panel/user-details/user-details.component';
import {UserModifyContentComponent} from './user-panel/user-modify-content/user-modify-content.component';
import {UserBuyContentsComponent} from './user-panel/user-buy-contents/user-buy-contents.component';
import {UserContentsComponent} from './user-panel/user-contents/user-contents.component';
import {SitemapComponent} from './sitemap/sitemap.component';
import {UserFinancialAccountComponent} from './user-panel/user-financials/user-financial-account/user-financial-account.component';
import {UserPaidTransactionsComponent} from './user-panel/user-financials/user-paid-transactions/user-paid-transactions.component';
import {UserPayerTransactionsComponent} from './user-panel/user-financials/user-payer-transactions/user-payer-transactions.component';
import {UserPonyComponent} from './user-panel/user-financials/user-pony/user-pony.component';
import {AboutusComponent} from "./aboutus/aboutus.component";
/**
 * Created by iman on 6/13/2017.
 */

export const appRoutes: Routes = [

  { path: '', component: MainBookletComponent, pathMatch: 'full' },
  { path: 'content/list/:group', component: BookletListComponent },
  { path: 'content/cat/:id', component: BookletListComponent },
  { path: 'content/cat/:id/:name', component: BookletListComponent },
  { path: 'content/tag/:id', component: BookletListComponent },
  { path: 'content/tag/:id/:name', component: BookletListComponent },
  { path: 'content/search', component: BookletListComponent },

  { path: 'content/:uid/:name', component: BookletDetailsComponent },
  { path: 'content/:uid', component: BookletDetailsComponent },




  { path: 'admin', component: AdminPannelComponent},
  { path: 'admin/content/new', component: ModifyBookletComponent},
  { path: 'admin/content/edit/:id', component: ModifyBookletComponent },
  { path: 'admin/comments', component: CommentsListComponent},
  { path: 'admin/contents', component: AdminBookletListComponent },
  // canActivate: [AuthGuard]

  { path: 'panel/user/details', component: UserDetailsComponent},
  { path: 'panel/user/newContent', component: UserModifyContentComponent},
  { path: 'panel/user/edit/:id', component: UserModifyContentComponent},
  { path: 'panel/user/myContents', component: UserContentsComponent},
  { path: 'panel/user/buyContents', component: UserBuyContentsComponent},

  { path: 'panel/user/financial/account', component: UserFinancialAccountComponent},
  { path: 'panel/user/financial/as-paid-transactions', component: UserPaidTransactionsComponent},
  { path: 'panel/user/financial/as-payer-transactions', component: UserPayerTransactionsComponent},
  { path: 'panel/user/financial/pony', component: UserPonyComponent},

  { path: 'sitemap', component: SitemapComponent},


  { path: 'terms', component: TermComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'about-us', component: AboutusComponent },
  { path: 'AccessDenied', component: AccessDeniedComponent }
  // { path: 'home/:city', component: MainPageComponent,children:ChildRoutes}

];
