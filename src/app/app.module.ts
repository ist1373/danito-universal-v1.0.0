// import { BrowserModule } from '@angular/platform-browser';
import {ErrorHandler, NgModule, NgZone, NO_ERRORS_SCHEMA} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { HeaderComponent} from './header/header.component';
import { SliderComponent } from './slider/slider.component';
import { FooterComponent } from './footer/footer.component';
import { BookletComponent } from './main-booklet/booklet/booklet.component';
import { BookletCarouselComponent } from './main-booklet/booklet-carousel/booklet-carousel.component';
import { MainBookletComponent } from './main-booklet/main-booklet.component';
import { SpecialBookletComponent } from './main-booklet/special-booklet/special-booklet.component';
import { CategorySectionComponent } from './main-booklet/category-section/category-section.component';
import { CategoryItemComponent } from './main-booklet/category-section/category-item/category-item.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MdCheckboxModule,
  MdChipsModule,
  MdDialogModule, MdProgressBarModule, MdSlideToggleModule, MdSnackBarModule, MdTabsModule,
  MdTooltipModule
} from '@angular/material';
import { BookletListComponent } from './main-booklet/booklet-list/booklet-list.component';
import { BookletDetailsComponent } from './main-booklet/booklet-details/booklet-details.component';
import { StarRatingModule } from 'angular-star-rating';
import { TermComponent } from './term/term.component';
import {CategoryDialogComponent} from './category-dialog/category-dialog.component';
import { LoginRegisterComponent } from './login-register/login-register.component';
import {CacheService, LocalStorageServiceModule} from 'ng2-cache-service';
import {ProgressHttpModule} from 'angular-progress-http';
import {GalleryConfig, GalleryModule} from 'ng-gallery';
import { ModifyBookletComponent } from './admin-pannel/modify-booklet/modify-booklet.component';
import {RlTagInputModule} from 'angular2-tag-input/dist';
import {SlimLoadingBarModule} from 'ng2-slim-loading-bar';
import {HttpInterceptorModule} from 'ng-http-interceptor';
import {BrowserModule} from '@angular/platform-browser';
import {CacheStorageAbstract} from 'ng2-cache-service/dist/src/services/storage/cache-storage-abstract.service';
import {CacheLocalStorage} from 'ng2-cache-service/dist/src/services/storage/local-storage/cache-local-storage.service';
import {appRoutes} from './app.routes';
import {AuthGuard} from './guards/auth.guard';
import { AccessDeniedComponent } from './access-denied/access-denied.component';
import { AdminPannelComponent } from './admin-pannel/admin-pannel.component';
import { AdminHeaderComponent } from './admin-pannel/admin-header/admin-header.component';
import { CommentsListComponent } from './admin-pannel/comments-list/comments-list.component';
import { AdminBookletListComponent } from './admin-pannel/admin-booklet-list/admin-booklet-list.component';
import { AdminBookletComponent } from './admin-pannel/admin-booklet/admin-booklet.component';
import {SelectModule} from 'ng-select';
import {CommonModule} from '@angular/common';
import { UserHeaderComponent } from './user-panel/user-header/user-header.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { UserDetailsComponent } from './user-panel/user-details/user-details.component';
import { UserContentsComponent } from './user-panel/user-contents/user-contents.component';
import { UserBuyContentsComponent } from './user-panel/user-buy-contents/user-buy-contents.component';
import { UserModifyContentComponent } from './user-panel/user-modify-content/user-modify-content.component';
import { UserContentComponent } from './user-panel/user-content/user-content.component';
import { SitemapComponent } from './sitemap/sitemap.component';
import { UserFinancialHeaderComponent } from './user-panel/user-financials/user-financial-header/user-financial-header.component';
import { UserFinancialAccountComponent } from './user-panel/user-financials/user-financial-account/user-financial-account.component';
import { UserPaidTransactionsComponent } from './user-panel/user-financials/user-paid-transactions/user-paid-transactions.component';
import { UserPayerTransactionsComponent } from './user-panel/user-financials/user-payer-transactions/user-payer-transactions.component';
import { UserPonyComponent } from './user-panel/user-financials/user-pony/user-pony.component';
import {TagInputModule} from "ngx-chips";
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import {FileSizePipe} from "./utils/FileSizePipe";
import { FactorDialogComponent } from './factor-dialog/factor-dialog.component';
import {Ng2DeviceDetectorModule} from "ng2-device-detector";
import { AboutusComponent } from './aboutus/aboutus.component';
import {LazyLoadImageModule} from "ng-lazyload-image";
import {MetaModule} from "@ngx-meta/core";
import {OwlModule} from "ngx-owl-carousel";
import {PersainNumberPipe} from "./utils/persain-number.pipe";
import {JalaaliPipe} from "./utils/JalaaliPipe";




export const galleryConfig : GalleryConfig = {
  "style": {
    "background": "#121519",
    "width": "100%",
    "height": "100%",
    "padding": "1em"
  },
  "animation": "fade",
  "loader": {
    "width": "50px",
    "height": "50px",
    "position": "center",
    "icon": "oval"
  },
  "description": {
    "position": "top",
    "overlay": false,
    "text": false,
    "counter": true
  },
  "bullets": {
    "position": "bottom"
  },
  "player": {
    "autoplay": false,
    "speed": 3000
  },
  "thumbnails": {
    "width": 120,
    "height": 90,
    "position": "bottom",
    "space": 20
  }
}

@NgModule({
  schemas:[NO_ERRORS_SCHEMA],
  declarations: [
    JalaaliPipe,
    FileSizePipe,
    PersainNumberPipe,
    AppComponent,
    HeaderComponent,
    SliderComponent,
    FooterComponent,
    BookletComponent,
    BookletCarouselComponent,
    MainBookletComponent,
    SpecialBookletComponent,
    CategorySectionComponent,
    CategoryItemComponent,
    BookletListComponent,
    BookletDetailsComponent,
    TermComponent,
    CategoryDialogComponent,
    LoginRegisterComponent,
    ModifyBookletComponent,
    AccessDeniedComponent,
    AdminPannelComponent,
    AdminHeaderComponent,
    CommentsListComponent,
    AdminBookletListComponent,
    AdminBookletComponent,
    UserHeaderComponent,
    ContactUsComponent,
    UserDetailsComponent,
    UserContentsComponent,
    UserBuyContentsComponent,
    UserModifyContentComponent,
    UserContentComponent,
    SitemapComponent,
    UserFinancialHeaderComponent,
    UserFinancialAccountComponent,
    UserPaidTransactionsComponent,
    UserPayerTransactionsComponent,
    UserPonyComponent,
    FactorDialogComponent,
    AboutusComponent
  ],
  entryComponents: [CategoryDialogComponent, LoginRegisterComponent,FactorDialogComponent],
  imports: [
    Ng2DeviceDetectorModule.forRoot(),
    BrowserModule.withServerTransition({appId: 'angular-universal'}),
    RlTagInputModule,
    TagInputModule,
    HttpInterceptorModule,
    MdChipsModule,
    FormsModule,
    ReactiveFormsModule,
    MdSnackBarModule,
    HttpModule,
    CommonModule,
    OwlModule,
    BrowserAnimationsModule,
    MdTooltipModule,
    MdTabsModule,
    MdDialogModule,
    StarRatingModule,
    ProgressHttpModule,
    SelectModule,
    MdSlideToggleModule,
    MdCheckboxModule,
    MdProgressBarModule,
    LocalStorageServiceModule,
    LazyLoadImageModule,
    MetaModule.forRoot(),
    SlimLoadingBarModule.forRoot(),
    GalleryModule.forRoot(galleryConfig),
    RouterModule.forRoot(appRoutes),
  ],
  providers: [  {provide: CacheStorageAbstract, useClass: CacheLocalStorage}, AuthGuard, CacheService],
  bootstrap: [AppComponent]

})
export class AppModule { }

