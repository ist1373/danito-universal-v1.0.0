/**
 * Created by iman on 09/02/2017.
 */

// export var AppContextUrl = "http://185.94.99.210/api/v1/";


export var AppContextUrl = 'http://api.booksum.ir/api/v1/';
export var AppContextUrlV2 = 'http://api.booksum.ir/api/v2/';
// export var AppContextUrl = "http://localhost:8080/api/v1/";
// export var AppContextUrlV2 = "http://localhost:8080/api/v2/";
export var FileContextUrl = 'http://file.booksum.ir/download_file.php?';
// export var ContentContextUrl = 'http://file.booksum.ir/download_content.php?';
export var ContentContextUrl = 'http://file.booksum.ir/download_content_2.php?';


export var PaymentZarinpal="https://www.zarinpal.com/pg/rest/WebGate/PaymentRequest.json";
export var MerchantID="9f81a8ce-3e4a-11e7-be9a-005056a205be";
export var ExpireTime = 604800000;
export var ItemSizePerPage = 12;
export var ItemsLoadForList = 100;
