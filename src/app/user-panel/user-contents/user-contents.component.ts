import { Component, OnInit } from '@angular/core';
import {Content} from "../../entity/Content";
import {BookletService} from "../../service/booklet.service";
import {isPlatformBrowser} from "@angular/common";
import {CacheService} from "ng2-cache-service";

@Component({
  selector: 'app-user-contents',
  templateUrl: './user-contents.component.html',
  styleUrls: ['./user-contents.component.scss'],
  providers:[BookletService,CacheService]
})
export class UserContentsComponent implements OnInit {

  page = 0;
  contents:Content[];
  hasMoreContents = true;

  constructor(private bookletService:BookletService,private cacheService:CacheService) { }

  ngOnInit() {
    if (isPlatformBrowser(this.cacheService.get('platform'))) {
      window.scrollTo(0, 0);
    }
    this.loadDownloadedContent(0,false);
  }

  loadMore(){
    this.page++;
    this.loadDownloadedContent(this.page,true);
  }

  loadDownloadedContent(page,more:boolean)
  {
    if (!more)
    {
      this.bookletService.getMyContents(page).subscribe(contents=>{
        this.contents = contents;
      });
    }
    else
    {
      this.bookletService.getMyContents(page).subscribe(contents=>{
        if(contents.length == 0)
        {
          this.hasMoreContents ==false;
        }
        for(let content of contents)
        {
          this.contents.push(content);
        }
      });
    }
  }


}
