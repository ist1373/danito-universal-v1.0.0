import {Component, Input, OnInit} from '@angular/core';
import {Content, PersianProductState, ProductState} from "../../entity/Content";
import {AppContextUrl, FileContextUrl} from "../../globals";
import {BookletService} from "../../service/booklet.service";



@Component({
  selector: 'app-user-content',
  templateUrl: './user-content.component.html',
  styleUrls: ['./user-content.component.scss'],
  providers:[BookletService]

})
export class UserContentComponent implements OnInit {
  @Input() booklet:Content;
  base=AppContextUrl;
  baseFile = FileContextUrl;


  userAccessStates: Array<any>;

  ProductState: typeof ProductState = ProductState;
  PersianProductState: typeof PersianProductState = PersianProductState;
  newState:ProductState ;

  constructor(private bookletService:BookletService){

  }

  ngOnInit() {
    this.userAccessStates = [
      {value: '1', label: 'منتشر شده'},
      {value: '4', label: 'آرشیو شده'}
    ];

    this.newState= this.booklet.productState;
    console.log(this.booklet.productState.toString() == ProductState[ProductState.Evaluating])
  }

  changeJozveState(event:any,booklet:Content)
  {
    this.newState = event.value;
  }
  editJozveState(booklet:Content){

    this.bookletService.editJozveState(booklet.id,this.newState).subscribe((res)=>{
      console.log(res);

    });


  }


}
