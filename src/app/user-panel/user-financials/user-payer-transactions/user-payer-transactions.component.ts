import { Component, OnInit } from '@angular/core';
import {TransactionService} from "../../../service/transaction.service";
declare var Jalaali:any;

@Component({
  selector: 'app-user-payer-transactions',
  templateUrl: './user-payer-transactions.component.html',
  styleUrls: ['./user-payer-transactions.component.scss'],
  providers: [TransactionService]
})
export class UserPayerTransactionsComponent implements OnInit {


  payments = [];
  constructor(private transactionService : TransactionService) { }

  ngOnInit() {


    this.transactionService.getUserAsPayerTransactions(0).subscribe((trans) => {
      console.log(trans);
      this.payments = trans;
    });
  }



}
