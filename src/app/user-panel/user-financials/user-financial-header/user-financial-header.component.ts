import { Component, OnInit } from '@angular/core';
import {CacheService} from "ng2-cache-service";

@Component({
  selector: 'app-user-financial-header',
  templateUrl: './user-financial-header.component.html',
  styleUrls: ['./user-financial-header.component.scss']
})
export class UserFinancialHeaderComponent implements OnInit {

  user = this.cacheService.get('user');

  constructor(private cacheService:CacheService) { }

  ngOnInit() {
  }

  checkUserAuthority(authority:string)
  {
    if(this.user != null)
    {
      for (let auth of this.user.authorities)
      {
        if (auth.name == authority)
          return true;
      }
      return false;
    }
    else{
      return false;
    }
  }
}
