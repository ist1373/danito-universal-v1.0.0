import { Component, OnInit } from '@angular/core';
import {TransactionService} from "../../../service/transaction.service";
import {isPlatformBrowser} from "@angular/common";
import {CacheService} from "ng2-cache-service";

@Component({
  selector: 'app-user-paid-transactions.component',
  templateUrl: './user-paid-transactions.component.html',
  styleUrls: ['./user-paid-transactions.component.scss'],
  providers:[TransactionService,CacheService]
})
export class UserPaidTransactionsComponent implements OnInit {
  transactions = [];
  constructor(private transactionService : TransactionService,private cacheService:CacheService) { }

  ngOnInit() {
    if (isPlatformBrowser(this.cacheService.get('platform'))) {
      window.scrollTo(0, 0);
    }

    this.transactionService.getUserAsPaidTransactions(0).subscribe((trans) => {
      console.log(trans);
      this.transactions = trans;
    });
  }


}
