import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {Student} from "../../../entity/Student";
import {CacheService} from "ng2-cache-service";
import {AppContextUrl, FileContextUrl} from "../../../globals";
import {RequestOptions,Headers} from "@angular/http";
import {ProgressHttp} from "angular-progress-http";
import {UserService} from "../../../service/user.service";
import {MdSnackBar, MdSnackBarConfig} from "@angular/material";
import {UploadedFile} from "../../../entity/UploadedFile";
import {isPlatformBrowser} from "@angular/common";

@Component({
  selector: 'app-user-financial-account',
  templateUrl: './user-financial-account.component.html',
  styleUrls: ['./user-financial-account.component.scss']
})
export class UserFinancialAccountComponent implements OnInit {
  base=AppContextUrl;
  baseFile = FileContextUrl;
  token = this.cacheService.get('token');
  user:Student = this.cacheService.get('user');
  financialForm: FormGroup;
  national = new FormControl('');
  postal = new FormControl('');
  shaba = new FormControl('');
  address = new FormControl('');
  nationalCardUplaodPercent = 0;
  constructor(private cacheService:CacheService,private http:ProgressHttp,private userService:UserService,
              public snackBar: MdSnackBar) {
    this.financialForm = new FormGroup({
      national: this.national,
      postal: this.postal,
      shaba: this.shaba,
      address:this.address
    });
  }



  ngOnInit() {
    if (isPlatformBrowser(this.cacheService.get('platform'))) {
      window.scrollTo(0, 0);
    }
    this.national.setValue(this.user.nationalCode);
    this.postal.setValue(this.user.postalCode);
    this.shaba.setValue(this.user.shabaCode);
    this.address.setValue(this.user.address);
  }


  openFileBrowser(event)
  {

    let file = event.target.files[0];
    if(file != null)
    {
      var uploadUrl= AppContextUrl+'file/upload';

      let formData:FormData = new FormData();
      formData.append('file', file, file.name);
      formData.append('accessType', 'Private' );
      let headers = new Headers({});
      headers.set('Authorization',this.cacheService.get('token'));
      let options = new RequestOptions({ headers: headers});

      this.http
        .withUploadProgressListener(progress => {
          this.nationalCardUplaodPercent = progress.percentage;
        })
        .post(uploadUrl,formData,options)
        .subscribe((response) => {
          console.log(response);


          var uploadedFile = new UploadedFile();
          uploadedFile.id = response.json().code;
          uploadedFile.description =response.json().message + response.json().code;
          uploadedFile.size = file.size;
          this.user.nationalCardImage = uploadedFile;
          this.cacheService.set('user',this.user);
          this.userService.editUser(this.user).subscribe((res) => {
            console.log(res);
            if(res.json()['success']==true)
            {
              this.openSnackBar("ارسال عکس با موفقیت انجام شد.","success");
            }
            else
              this.openSnackBar(res.json()['message'],"error");
          });
        })
    }
  }

  saveUserDetails(){
      this.user.shabaCode = this.shaba.value;
      this.user.postalCode = this.postal.value;
      this.user.nationalCode = this.national.value;
      this.user.address = this.address.value;
      this.user.password = null;
      this.cacheService.set('user',this.user);
      this.userService.editUser(this.user).subscribe((res) => {
        console.log(res);
        if(res.json()['success']==true)
        {
          this.openSnackBar("اطلاعات شما با موفقیت ثبت شد.","success");
        }
        else
          this.openSnackBar(res.json()['message'],"error");
      });
  }

  openSnackBar(message:string,cssClass:string) {
    var conf = new MdSnackBarConfig();
    conf.duration = 5000;
    conf.extraClasses = [cssClass];
    this.snackBar.open(message,'',conf);
  }
}
