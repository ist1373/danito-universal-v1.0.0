import { Component, OnInit } from '@angular/core';
import {BookletService} from "../../service/booklet.service";
import {Content} from "../../entity/Content";

@Component({
  selector: 'app-user-buy-contents',
  templateUrl: './user-buy-contents.component.html',
  styleUrls: ['./user-buy-contents.component.scss'],
  providers:[BookletService]
})
export class UserBuyContentsComponent implements OnInit {

  page = 0;
  contents:Content[];
  hasMoreContents = true;

  constructor(private bookletService:BookletService) { }

  ngOnInit() {
    console.log("sdfsdf");
    this.loadDownloadedContent(0,false);
  }

  loadMore(){
    this.page++;
    this.loadDownloadedContent(this.page,true);
  }

  loadDownloadedContent(page,more:boolean)
  {
    if (!more)
    {
      console.log("sdfsdf");
      this.bookletService.getDownloadedContentsByUser(page).subscribe(contents=>{
        console.log(contents);
         this.contents = contents;
      });
    }
    else
    {
      this.bookletService.getDownloadedContentsByUser(page).subscribe(contents=>{
        if(contents.length == 0)
        {
          this.hasMoreContents ==false;
        }
        for(let content of contents)
        {
          this.contents.push(content);
        }
      });
    }
  }


}
