import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {UniversityService} from "../../service/university.service";
import {University} from "../../entity/University";
import {CacheService} from "ng2-cache-service";
import {number} from "ng2-validation/dist/number";
import {strictEqual} from "assert";
import {AppContextUrl, FileContextUrl} from "../../globals";
import {ProgressHttp} from "angular-progress-http";
import {RequestOptions,Headers,Response} from "@angular/http";
import {AccessType, UploadedFile} from "../../entity/UploadedFile";
import {Content, ContentType} from "../../entity/Content";
import {CategoryService} from "../../category-dialog/category-service";
import {MdDialog, MdSnackBar, MdSnackBarConfig} from "@angular/material";
import {CategoryDialogComponent} from "../../category-dialog/category-dialog.component";
import {plainToClass} from "class-transformer";
import {Category} from "../../entity/Category";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {User} from "../../entity/User";
import {TagService} from "../../service/tag.service";
import {Tag} from "../../entity/Tag";
import {TagContentType} from "@angular/compiler";
import {BookletService} from "../../service/booklet.service";
import {ActivatedRoute} from "@angular/router";
import {GalleryService} from "ng-gallery";
import {isPlatformBrowser} from "@angular/common";





@Component({
  selector: 'app-user-modify-content',
  templateUrl: './user-modify-content.component.html',
  styleUrls: ['./user-modify-content.component.scss'],
  providers:[UniversityService,CacheService,CategoryService,BookletService,TagService]
})
export class UserModifyContentComponent implements OnInit {

  modificationType:ModificationType;
  category:Category;
  content:Content;
  universities : Array<any>=[];
  files:File[] = []; // new files have to be uploaded

  sampleImages:File[]=[];
  fileProccesses:ProgressUpload[]=[];
  imagesProgresses:ProgressUpload[]=[];
  fileAccessType : Array<any>;
  private value:any = {};


  FileType: typeof FileType = FileType;
  ModificationType: typeof ModificationType = ModificationType;
  AccessType: typeof AccessType = AccessType;


  jozveForm:FormGroup;
  title = new FormControl('', Validators.required);
  price = new FormControl('', Validators.required);
  // writer = new FormControl('');
  writeYear = new FormControl('');
  pages = new FormControl('');
  prof = new FormControl('');
  desc = new FormControl('');
  base = AppContextUrl;
  baseFile = FileContextUrl;
  token = this.cacheService.get('token');
  sampleGalleryImages=[];



  public ngModelTags = [];// tags to save by tagModule
  public autocompleteBookletTags = [];// tags to offer in html




  constructor(private universityService:UniversityService, private cacheService:CacheService,
              private http: ProgressHttp, private categoryService:CategoryService, public dialog: MdDialog,
              private bookletService:BookletService, public snackBar: MdSnackBar, private tagService:TagService,
              private activatedRoute:ActivatedRoute,public gallery:GalleryService,private elementRef: ElementRef) {

    this.jozveForm=new FormGroup({
      title:this.title,
      price: this.price,
      // writer: this.writer,
      writeYear:this.writeYear,
      pages:this.pages,
      prof:this.prof,
      desc:this.desc});
  }




  ngOnInit() {
    if (isPlatformBrowser(this.cacheService.get('platform'))) {
      window.scrollTo(0, 0);
    }
    this.initUniversirsities();
    this.content = new Content();
    this.activatedRoute.url.subscribe((url)=>{
      if(url[2].toString()=='newContent')
      {
        this.modificationType = ModificationType.New;
      }
      else
      {
        this.modificationType = ModificationType.Edit;
        this.bookletService.getContentByID(Number(url[3])).subscribe((booklet)=>{
          this.content = booklet;
          console.log(booklet);
          this.initSampleImages();
          console.log(AccessType[booklet.files[0].accessType]);
          console.log(AccessType['AccessOnPay']);
          console.log(booklet.files[0].accessType);
          this.title.setValue(booklet.title) ;
          this.price.setValue(booklet.price);
          this.writeYear.setValue(booklet.writeYear);
          // this.writer.setValue(booklet.writer);
          this.pages.setValue(booklet.pagesNum);
          this.prof.setValue(booklet.professor);
          this.desc.setValue(booklet.description);
          this.category = booklet.category;
          for(let tag of booklet.tags)
            this.ngModelTags.push({display:tag.tagString,value:tag.id});
          // for(let file of booklet.files)
          //   this.existedFiles.push(file);
        });
      }
    });
    this.fileAccessType = [
      {value: '0', label: 'Public'},
      {value: '1', label: 'Private'},
      {value: '2', label: 'AccessOnPay'},
      {value: '3', label: 'Forbidden'}
    ];
  }




  initSampleImages()
  {
    for(let image of this.content.images)
    {
      let i={
        src:FileContextUrl+'fid='+image.id,
        thumbnail: FileContextUrl+'fid='+image.id+ '&isThumb=true',
        text: image.description
      };
      this.sampleGalleryImages.push(i);
    }
    console.log(this.sampleGalleryImages);
    this.gallery.load(this.sampleGalleryImages);
  }

  openFileBrowser(event,fileType:FileType)
  {
    if(fileType == FileType.Jozve)
    {
      for(let file of event.target.files)
      {
        this.files.push(file);
        this.fileProccesses.push(new ProgressUpload());
      }
    }
    else
    {
      for(let file of event.target.files)
      {
        this.sampleImages.push(file);
        this.imagesProgresses.push(new ProgressUpload());
      }
    }
  }
  uploadFile(file:File,fileType:FileType,index:number)
  {

    var uploadUrl= AppContextUrl+'file/upload';

    let formData:FormData = new FormData();
    formData.append('file', file, file.name);
    if(this.content.price==0||fileType==FileType.Image)
    {
      formData.append('accessType', 'Public' );
    }
    else
    {
      formData.append('accessType', 'AccessOnPay' );
    }
    let headers = new Headers({});
    headers.set("Authorization",this.cacheService.get("token"));
    let options = new RequestOptions({ headers: headers});

    this.http
      .withUploadProgressListener(progress => {
        if(fileType==FileType.Jozve)
          this.fileProccesses[index].progress=progress.percentage;
        else
          this.imagesProgresses[index].progress=progress.percentage;
      })
      .post(uploadUrl,formData,options)
      .subscribe((response) => {
        console.log(response);

        var uploadedFile = new UploadedFile();
        uploadedFile.id = response.json().code;
        uploadedFile.description =response.json().message + response.json().code;
        uploadedFile.size = file.size;
        if(fileType==FileType.Jozve)
        {
          this.content.files.push(uploadedFile);
          this.fileProccesses[index].complete = true;
        }
        else
        {
          this.content.images.push(uploadedFile);
          this.imagesProgresses[index].complete=true;
        }

      })
  }


  openCategoryModal(){
    this.categoryService.getParentsCategory().subscribe((data:Response)=> {
      let dialogRef = this.dialog.open(CategoryDialogComponent);
      dialogRef.componentInstance.returnCatId = true;
      dialogRef.componentInstance.categories = plainToClass(Category, data.json());
      dialogRef.componentInstance.categoryHeader ="دسته بندی";
    });
  }

  ngAfterViewInit(){
    CategoryService.categoryObserver.subscribe((cat:Category)=>{
      this.category = cat;
      this.content.category = cat
    });
  }

  saveJozve()
  {
    this.content.contentType = ContentType.Booklet;
    this.content.title = this.title.value;
    this.content.price = this.price.value;
    // this.book.writer = this.writer.value;
    this.content.pagesNum = this.pages.value as number;
    this.content.writeYear = this.writeYear.value;
    let user:User = this.cacheService.get("user");
    this.content.description = this.desc.value;
    this.content.professor = this.prof.value;
    console.log(this.content);
    this.bookletService.saveJozve(this.content).subscribe((res)=>{
      console.log(res);
      if(res.json()['success']==true)
      {
        this.openSnackBar("ذخیره جزوه با موفقیت انجام شد","success");
        this.clearForms();
      }
      else
        this.openSnackBar(res.json()['message'],"error");
    });
  }

  editJozve(){
    console.log(this.content);
    this.content.title = this.title.value;
    this.content.price = this.price.value;
    // this.book.writer = this.writer.value;
    this.content.pagesNum = this.pages.value as number;
    this.content.writeYear = this.writeYear.value;
    let user:User = this.cacheService.get("user");
    this.content.description = this.desc.value;
    this.bookletService.editJozve(this.content).subscribe((res)=>{
      console.log(res);
      if(res.json()['success']==true)
        this.openSnackBar("ذخیره جزوه با موفقیت انجام شد","success");
      else
        this.openSnackBar(res.json()['message'],"error");
    });
  }



  getAutoCompleteTags(tagStr)
  {
    if(tagStr.length>0)
    {
      this.tagService.getSimilarTages(tagStr).subscribe((tags:Tag[])=>{
        this.autocompleteBookletTags = [];
        for (let tag of tags)
        {
          // this.tags.set(tag.tagString,tag.id);
          let atag = {display:tag.tagString,value:tag.id}

          this.autocompleteBookletTags.push(atag);
        }
      })
    }
  }

  onRemoveTag(removeTag){
    console.log(removeTag);
    for(let i =0;i<this.content.tags.length;i++ )
    {
      let tag = this.content.tags[i];
      if(tag.tagString == removeTag.display)
      {
        this.content.tags.splice(i,1);

      }
    }
  }

  onAddTag(addedTag)
  {
    console.log(addedTag);
    let tag = new Tag();
    tag.tagString = addedTag.display;
    tag.id = addedTag.value;
    this.content.tags.push(tag);
  }


  openSnackBar(message:string,cssClass:string) {
    var conf = new MdSnackBarConfig();
    conf.duration = 5000;
    conf.extraClasses = [cssClass];
    this.snackBar.open(message,"",conf);
  }

  initUniversirsities()
{

  this.universityService.getUniversities().subscribe((unis:University[])=>{
    this.universities.push({value:0,label:'انتخاب نشده'});
    for(let uni of unis)
    {
      this.universities.push({value:uni.id,label:uni.title.toString()});
    }
  });
  this.cacheService.set("unis",this.universities);
}
  public selectUniversity(item:any):void {
    if(item.value != 0)
    {
      console.log('Selected value is: ', item.value);
      let uni = new University();
      uni.id = item.value;
      this.content.university = uni;
    }
    else {
      this.content.university = null;
    }

  }




  changeFileAcessType(event,i:number)
  {

    this.content.files[i].accessType = this.fileAccessType[event.value].label;
  }
  changeImageAcessType(event,i:number)
  {
    this.content.images[i].accessType = this.fileAccessType[event.value].label;
  }

  clearForms(){
    this.title.setValue('');
    this.pages.setValue('');
    this.prof.setValue('');
    this.desc.setValue('');
    this.ngModelTags = [];
    this.category = null;
    this.files = [];
    this.price.setValue('');
    this.writeYear.setValue('');
    this.sampleImages = [];
    this.imagesProgresses = [];
    this.content = new Content();
    this.elementRef.nativeElement.querySelector('ng-select').select('0');
  }

}

export enum FileType{Jozve,Image};
export enum ModificationType{Edit,New};

export class ProgressUpload{
  progress:number=0;
  complete:boolean=false;

}
