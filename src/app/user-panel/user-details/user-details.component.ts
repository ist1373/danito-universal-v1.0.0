import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AppContextUrl, FileContextUrl} from "../../globals";
import {CacheService} from "ng2-cache-service";
import {User} from "../../entity/User";
import {CustomValidators} from "ng2-validation";
import {RequestOptions, Headers, Http} from "@angular/http";
import {UploadedFile} from "../../entity/UploadedFile";
import {HttpWithUploadProgressListener, ProgressHttp} from "angular-progress-http";
import {UserService} from "../../service/user.service";
import {UniversityService} from "../../service/university.service";
import {University} from "../../entity/University";
import {Student} from "../../entity/Student";
import {LocationService} from "../../service/location.service";
import {Province} from "../../entity/Province";
import {City} from "../../entity/City";
import {MdSnackBar, MdSnackBarConfig} from "@angular/material";
import {isPlatformBrowser} from "@angular/common";

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss'],
  providers:[CacheService,UserService,LocationService]
})
export class UserDetailsComponent implements OnInit {

  base = AppContextUrl;
  baseFile = FileContextUrl;
  userDetails:FormGroup;
  username=new FormControl('');
  firstname=new FormControl('');
  lastname=new FormControl('');
  universities = [];


  pass = new FormControl('', Validators.required);
  passrep = new FormControl('', CustomValidators.equalTo(this.pass));
  provinces= [];
  cities = [];
  email=new FormControl('',CustomValidators.email);
  biography=new FormControl('');
  phone=new FormControl('',CustomValidators.digits);
  avatarUploadPercent = 0;
  token = this.cacheService.get('token');
  user:Student = this.cacheService.get("user");


  constructor(private cacheService:CacheService,private http:ProgressHttp,private userService:UserService,
  private universityService:UniversityService,private locationService:LocationService,public snackBar: MdSnackBar)
  {
    this.userDetails = new FormGroup({
      username:this.username,
      firstname:this.firstname,
      lastname:this.lastname,
      email:this.email,
      phone:this.phone,
      pass:this.pass,
      passrep:this.passrep,
      biography:this.biography
    });
  }


  ngOnInit() {
    console.log(this.user);
    if (isPlatformBrowser(this.cacheService.get('platform'))) {
      window.scrollTo(0, 0);
    }
    this.username.setValue(this.user.username);
    this.firstname.setValue(this.user.firstName);
    this.lastname.setValue(this.user.lastName);
    this.email.setValue(this.user.email);
    this.phone.setValue(this.user.phoneNumber);
    this.biography.setValue(this.user.biography);

    this.initUniversirsities();
    this.initProvinces();
    this.cities.push({value:0,label:''})
  }




  openFileBrowser(event)
  {

    let file = event.target.files[0];
    if(file != null)
    {
      var uploadUrl= AppContextUrl+'file/upload';

      let formData:FormData = new FormData();
      formData.append('file', file, file.name);
      formData.append('accessType', 'Private' );
      let headers = new Headers({});
      headers.set('Authorization',this.cacheService.get('token'));
      let options = new RequestOptions({ headers: headers});

      this.http
        .withUploadProgressListener(progress => {
          this.avatarUploadPercent = progress.percentage;
        })
        .post(uploadUrl,formData,options)
        .subscribe((response) => {
          console.log(response);

          this.avatarUploadPercent = 0;
          var uploadedFile = new UploadedFile();
          uploadedFile.id = response.json().code;
          uploadedFile.description =response.json().message + response.json().code;
          uploadedFile.size = file.size;
          this.user.profileImage = uploadedFile;
          this.cacheService.set('user',this.user);
          this.userService.editUser(this.user).subscribe((res) => {
            console.log(res);
            if(res.json()['success']==true)
            {
              this.openSnackBar("اطلاعات شما با موفقیت ثبت شد.","success");
            }
            else
              this.openSnackBar(res.json()['message'],"error");
          });
        })
    }
  }


  initUniversirsities()
  {
    this.universityService.getUniversities().subscribe((unis:University[])=>{
      this.universities.push({value:0,label:'انتخاب نشده'});
      for(let uni of unis)
      {
        this.universities.push({value:uni.id,label:uni.title.toString()});
      }
    });


  }

  public selectUniversity(item:any):void {
    if(item.value != 0)
    {
      console.log('Selected value is: ', item.value);
      let uni = new University();
      uni.id = item.value;
      uni.title = item.label;
      this.user.university = uni;
    }
    else {

    }
  }


  initProvinces(){
    this.locationService.getProvinces().subscribe(provs =>{
      for(let prov of provs)
      {
        this.provinces.push({value:prov.id,label:prov.title.toString()})
      }
    })

  }
  public selectProvince(item:any):void {
    console.log('Selected value is: ', item.value);
    this.cities = [];
    this.getCitis(item.value);
    let prov = new Province();
    prov.id = item.value;
    prov.title = item.label;
    this.user.province = prov;
  }


  getCitis(provinceId:number){

    this.locationService.getCities(provinceId).subscribe(cities =>{
      console.log(cities);
      for(let city of cities)
      {
        this.cities.push({value:city.id,label:city.title})
      }
    });
  }

  public selectCity(item:any):void {
    console.log('Selected value is: ', item.value);
    let city = new City();
    city.id = item.value;
    city.title = item.label;
    this.user.city = city;
  }

  saveUserDetails(){

    if(this.pass.value.length!= 0)
    {
      this.user.password = this.pass.value;
        console.log('aldsfjsdfsadf')
    }
    else {
      this.user.password = null;
    }
    this.user.email = this.email.value;
    this.user.username = this.username.value;
    this.user.phoneNumber = this.phone.value;
    this.user.biography = this.biography.value;
    this.user.firstName = this.firstname.value;
    this.user.lastName = this.lastname.value;
    this.cacheService.set('user',this.user);
    this.userService.editUser(this.user).subscribe((res) => {
      console.log(res);
      if(res.json()['success']==true)
      {
        this.openSnackBar("اطلاعات شما با موفقیت ثبت شد.","success");
      }
      else
        this.openSnackBar(res.json()['message'],"error");
    });

  }

  openSnackBar(message:string,cssClass:string) {
    var conf = new MdSnackBarConfig();
    conf.duration = 5000;
    conf.extraClasses = [cssClass];
    this.snackBar.open(message,'',conf);
  }

}
