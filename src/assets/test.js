/**
 * Created by iman on 08/05/2017.
 */

var person = {
  firstName: "John",
  lastName : "Doe",
  id       : 5566,
  fullName : function() {
    return this.firstName + " " + this.lastName;
  }
};

var init = function () {
  new WOW().init();
}
